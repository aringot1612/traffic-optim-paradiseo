# Optimisation : Simulation de traffic (DEVS-STAR et ParadisEO)

## Auteur
- [Ringot Arthur](https://gitlab.com/aringot1612)

<br>

## Rapport de projet (au format pdf) : [Cliquez ici](https://gitlab.com/aringot1612/traffic-optim-paradiseo/-/tree/main/report/report.pdf)

<br>

## Compilation et lancement (sous linux de préférence)

<br>

Exécution de la ligne suivante dans le répertoire projet pour les algorithmes mono-objectif : 

	sh ./run_SO.sh

Exécution de la ligne suivante dans le répertoire projet pour les algorithmes multi-objectif : 

	sh ./run_MO.sh

<br>

## 1. Optimisation mono-objective.

<br>

### Question 1.a.

<br>

Il a été décidé d'opter pour une seule fonction objective afin d’évaluer les solutions proposées par les différents algorithmes :

Cette fonction est définie dans le fichier suivant.
```
/src/utils/evals/trafficEval.h
```
<br>

Elle prend en charge un paramètre, permettant de définir si la fitness à fournir doit correspondre au de temps ou au CO2 via un simple entier.
Nommé <b>type</b>, cet entier va prendre la valeur <b>1</b> (pour le temps) ou la valeur <b>2</b> (pour le co2).

<br>

Elle fait appel à un fichier supplémentaire : 
```
/src/utils/simulator.h
```
Ce dernier fichier fait appel au simulateur artis afin de calculer les valeurs de fitness pouvant être utilisées.

<br>

### Question 1.b.

<br>

Les algorithmes CMA-ES et DE sont disponibles ici :
```
/src/executables/cmaES.cpp
/src/executables/DE.cpp
```

Les algorithmes nécessitent d'autres fichiers, présents dans le dossier :
```
src/utils
```

<br>

Afin de réaliser des tests sur ces algorithmes.
Plusieurs expériences ont été réalisées.

Chaque algorithme a été testé avec 75 seed différentes avec pour seul critère d'arrêt : le temps (8 minutes).

Il est possible de personnaliser ces tests dans le fichier.
```
./run_SO.sh
```

<br>

Remarque :
Le lancement des algorithmes se fait en multithreading, Chaque seed implique le lancement en parallèle des deux algorithmes avec deux types d'évaluations.

Le temps total nécessaire aux tests est donc de 75 seed * 8 minutes : 10 heures.

<br>

Voici les graphiques obtenus : 
Ils permettent d'observer la tendance moyenne de la fitness à minimiser, ainsi que des boîtes à moustaches toutes les 10 générations, afin d'avoir une approche plus statistique.
Cela permet, entre autres, d'avoir une idée de l'écart-type entre les fitness, ainsi que de la présence de certaines valeurs aberrantes (symbolisées par de petits cercles).

<br>

#### Minimisation du temps

cmaES             |  DE
:-------------------------:|:-------------------------:
![](results/so/1_cmaES_.png)  |  ![](results/so/1_DE_.png)

<br>

#### Minimisation du co2

cmaES             |  DE
:-------------------------:|:-------------------------:
![](results/so/2_cmaES_.png)  |  ![](results/so/2_DE_.png)

<br>

Comme nous pouvons le remarquer.
Le DE se débrouille très bien, comparé à un cmaES.

En effet, nous avons, d'un côté, un cmaES incapable de faire converger les fitness entre les seed.
Avec beaucoup de valeurs aberrantes et un écart-type très important tout au long des générations.
La courbe des moyennes le montre également. Il n'y a aucune convergence.

<br>

Le DE, lui, fait converger très rapidement la fitness à minimiser, avec une vitesse de convergence plutôt élevée jusqu'à 50 générations.
De plus, le DE est très robuste (les boîtes à moustaches sont de plus en plus petites).

<br>

### Question 1.c.

<br>

Comme vu précédemment, le cmaES ne montre aucune convergence.
Le DE, lui, a une vitesse de convergence élevée jusqu'à 50 générations environ.
Une fois les 50 générations passées, la fitness moyenne converge de moins en moins vite, mais elle est de plus en plus précise.
Cette étude se base sur le nombre de générations, afin de faciliter la lecture du graphique.

<br>

Remarque : 
Tous les tests n'arrivent pas à accéder au même nombre de générations.
Il a été décidé de couper les générations non atteignables par certaines seed pour tous les graphiques.
En effet, cela pouvait fausser nos résultats.

En réalisant le traitement des données permettant d'obtenir ces graphiques, nous avons également cherché à trouver la meilleure fitness obtenue selon l'algorithme et selon le type d'évaluation.

<br>

Voici les résultats : 

```
cmaES (minimisation du temps)
	Algorithme : cmaES
	Seed : 1
	Generation : 1
	Fitness (temps) : 157083
	Solution : 24 1.8 0.3 1.8 0.1 1 1.8 1 1.3 0.1 0.4 1.3 1.3 30 20 30 35 20 35 35 35 30 20 35 35

DE (minimisation du temps) --> Meilleur
	Algorithme : DE
	Seed : 20
	Generation : 108
	Fitness (temps) : 156839
	Solution : 24 1.56475 0.3 1.32115 0.100017 0.77735 1.74044 1 1.03388 0.102096 0.394094 1.3 0.739432 29.9281 20 30 35 20 35 34.9873 34.2012 29.9222 20 35 34.8328

cmaES (minimisation du co2) --> Meilleur
	Algorithme : cmaES
	Seed : 1
	Generation : 11
	Fitness (co2) : 541720
	Solution : 24 1.2 0.3 1.8 0.1 1 1.8 1 1.3 0.3 0.4 0.7 0.7 20 10 20 25 10 25 25 25 20 10 30 30

DE (minimisation du co2)
	Algorithme : DE
	Seed : 4
	Generation : 77
	Fitness (co2) : 541919
	Solution : 24 1.2 0.3 1.8 0.101726 0.5 1.29228 1 1.20136 0.299395 0.372448 0.7 1.3 20 10.1676 20 25 10 25 25 25 20 10 30 30.0216
```

On peut remarquer que le cmaES détient la fitness la plus basse en minimisation du co2 alors qu'il n'est clairement pas le plus adapté par manque de robustesse.


#### Tests statistiques : P-Values selon la fitness

```
P-value entre l'algorithme cmaES et DE (Minimisation du temps) : 1.1918499863378202e-19
P-value entre l'algorithme cmaES et DE (Minimisation du co2) : 2.8578531694134255e-19

On remarque que les P-values sont très faibles.
Par conséquent, on peut donc dire que les résultats sont statistiquement significatifs.
```

<br>

## 2. Optimisation multi-objectif

<br>

### Question 2.a.

<br>

La fonction multi-objective est présente ici : 
```
/src/utils/evals/trafficEval/MO.h
```

Comme la fonction mono-objective, cette méthode fait appel au fichier <b>simulator.h</b>.
Son format est un peu différent car on cherche cette fois à récupérer deux fitness différentes : le temps ET le co2.

<br>

### Question 2.b - 2.d.

<br>

Les algorithmes nsgaII, IBEA et MOEA/D-DE sont disponibles ici : 
```
/src/executables/nsgaII.cpp
/src/executables/IBEA.cpp
/src/executables/moeaDDE.cpp
```

Afin de comparer les trois algorithmes, nous allons utiliser l'indicateur d'hypervolume.
Ce dernier est calculé via un script R :
```
R/script.R
```

L'ensemble des opérations nécessaires à l'étude des algorithmes est réalisé via le script suivant.
```
./run_MO.sh
```

On réalise un ensemble de tests identique aux algorithmes mono-objectif.
Et on ignore les générations qui ne sont pas atteignables par la majorité des seed.

Voici les graphiques montrant l'évolution de l'hypervolume au fil des générations.
Cet hypervolume doit être maximisé.

nsgaII  |  MOEA/D-DE  |  IBEA
:--:|:--:|:--:
![](results/mo/nsgaII.png)  |  ![](results/mo/MOEAD-DE.png)|  ![](results/mo/IBEA.png)

(On remarque ici la présence de l'algorithme MOEA/D-DE, plus de détails sur cet algorithme sont fournis plus bas.)

<br>

Entre les trois algorithmes, on remarque une vitesse de convergence très similaire.
Les boîtes à moustaches semblent dessiner une courbe quasi identique entre les algorithmes.

Néanmoins, c'est au niveau des écarts-types et des valeurs aberrantes que l'on peut constater des différences.

<br>

En effet, l'algorithme fournissant les résultats les moins "réguliers" est le nsgaII.
Ce dernier possède un bon nombre de valeurs d'hypervolume aberrantes, il est un peu moins robuste.
Le MOEA/D-DE semble faire un peu mieux.
Et enfin, l'IBEA se comporte très bien, avec une convergence d'hypervolume rapide et précise.

<br>

On remarque que le MOEA/D-DE n'atteint pas des valeurs d'hypervolumes aussi hautes que les deux autres algorithmes.
Mais cette différence est très légère.

<br>

Il a été décidé de rechercher la meilleure population obtenue pour chaque algorithme (uniquement d'après l'indicateur d'hypervolume).

Voici les différentes populations dans l'espace objectif.

nsgaII  |  MOEA/D-DE  |  IBEA
:--:|:--:|:--:
![](results/mo/best_nsgaII.png)  |  ![](results/mo/best_moeaDDE.png)|  ![](results/mo/best_IBEA.png)

<br>

On peut observer que le nsgaII fournit une population très variée.
l'IBEA : un peu moins et le MOEA/D-DE ne fournit que très peu d'individus différents (pour rappel, la population est fixée à 100).

<br>

Le manque de diversité de population dans le MOEA/D-DE est surement la cause d'un hypervolume moyen plus faible que les autres.

<br>

Voici les hypervolumes concernés : 

```
Algorithme nsgaII :
Meilleur hypervolume : 14727719228 (au bout de 97 générations de la seed 71)

Algorithme MOEA/D-DE :
Meilleur hypervolume : 14431071380 (au bout de 85 générations de la seed 60)

Algorithme IBEA :
Meilleur hypervolume : 14726605298 (au bout de 87 générations de la seed 61)

Ainsi, d'après l'hypervolume, le nsgaII détient la meilleure population.
```

<br>

#### Tests statistiques : P-Values selon l'hypervolume

```
P-value entre l'algorithme IBEA et MOEA/D-DE : 5.86819862946407e-14
P-value entre l'algorithme IBEA et nsgaII : 0.7838153639308514
P-value entre l'algorithme MOEA/D-DE et nsgaII : 2.3569174697756997e-11

Les P-values sont très faibles entre les résultats (MOEA/D-DE - IBEA) et (MOEA/D-DE - nsgaII).
Comme nous l'avons remarqué, le MOEA/D-DE ne fournit pas de très bons hypervolumes, comparé aux deux autres, d'où un ensemble de moyenne bien différent.

Néanmoins, entre l'IBEA et le nsgaII, les moyennes sont extrêmement proches : les deux algorithmes fournissent une courbe de moyenne quasi identique.
Par conséquent, la P-value est bien plus élevée.
```

<br>

### Question 2.c.

<br>

L'algorithme MOEA/D-DE a été développé selon le pseudo-code vu en cours.

<br>

Il nécessite un ensemble de fichier :
```
/src/executables/moeaDDE.cpp (fichier de « lancement »)
 ```

Ainsi que tous les fichiers dans le dossier : 
```
/src/utils/moeaDDE/
```
Le cœur de l'algorithme est présent ici.
```
/src/utils/moeaDDE/moeoMOEADDE.h
```

<br>

Ce dernier a une architecture proche des autres algorithmes Paradiseo de type moeo.
Un MOEA/D-DE intègre les mutations et croisement DE (utilisé en mono-objectif) : les fichiers correspondants sont donc également utilisés pour cet algorithme.

<br>

L'algorithme intégre une nouvelle manière d'initialiser une population.
Appliquée via le fichier :
```
/src/utils/moeaDDE/customInit.h
```

Cette initialisation de population est pour le moment désactivée.
Elle est réactivable en décommentant <b>les lignes 104 et 105 du fichier /src/executables/moeaDDE.cpp</b>

Cette méthode n'a pas donné de résultats probants.