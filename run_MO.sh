#!/bin/sh

maxSeed=75
time=480
pop=100
rm -r results/data/mo
rm -r results/mo
mkdir build
mkdir -p results/data/mo/hv
mkdir -p results/data/mo/algorithms
mkdir -p results/mo
cd build
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/usr/lib/pkgconfig
source $HOME/.bashrc
cmake -DCMAKE_INSTALL_PREFIX=$HOME/usr -DCMAKE_BUILD_TYPE=Release ..
make
clear

now=$(date +"%T")
echo "Start time: $now"

listAlgorithms="nsgaII IBEA moeaDDE"
algoSrc="./src/nsgaII ./src/IBEA ./src/moeaDDE"

for s in $(seq 1 $maxSeed)
do
	for a in $listAlgorithms
	do
		rm -r ../output/0/$s/$a
		mkdir -p ../output/0/$s/$a
	done
done

for s in $(seq 1 $maxSeed)
do
	now=$(date +"%T")
	echo "($now) -- Experiment $s/$maxSeed on all MO algorithms..."
	for a in $algoSrc
	do
		$a -S=$s -T=$time -P=$pop &
	done
	wait
done

now=$(date +"%T")
echo "($now) -- Preparing charts and results in progress..."
./src/multiObjectiveProcessing
cd ../R
R < script.R --save
cd ../build
./src/paretoFrontProcessing

now=$(date +"%T")
echo "($now) -- Creating charts and results in progress..."
cd ..
python3 mainMO.py

now=$(date +"%T")
echo "End time:   $now"