#!/bin/sh

maxSeed=75
time=480
pop=100
rm -r results/data/so
rm -r results/so
mkdir build
mkdir -p results/data/so
mkdir -p results/so
cd build
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$HOME/usr/lib/pkgconfig
source $HOME/.bashrc
cmake -DCMAKE_INSTALL_PREFIX=$HOME/usr -DCMAKE_BUILD_TYPE=Release ..
make
clear

now=$(date +"%T")
echo "Start time: $now"

algoSrc="./src/cmaES ./src/DE"
evalType="1 2"

for s in $(seq 1 $maxSeed)
do
	for i in $evalType
	do
		rm -r ../output/$i/$s
		mkdir -p ../output/$i/$s
	done
done

for s in $(seq 1 $maxSeed)
do
	now=$(date +"%T")
	echo "($now) -- Experiment $s/$maxSeed on all SO algorithms..."
	for a in $algoSrc
	do
		for e in $evalType
		do
			$a -S=$s -F=$e -T=$time -P=$pop &
		done
	done
	wait
done

now=$(date +"%T")
echo "($now) -- Preparing charts and results in progress..."
./src/singleObjectiveProcessing

echo "($now) -- Creating charts and results in progress..."
cd ..
python3 mainSO.py

now=$(date +"%T")
echo "End time:   $now"