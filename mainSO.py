from cProfile import label
import os
from fnmatch import fnmatch
import matplotlib.pyplot as plt3
import numpy as np
import scipy.stats as stats

timeTicks = np.arange(155000, 185000, 3000)
co2Ticks = np.arange(520000, 762000, 22000)

resultFileName = 'results/so/result.txt'

def exportResult(algorithm, evalType, bestData):
    dataSplit = bestData.split(',')
    tmpData = dataSplit[1]
    fWrite = open(resultFileName, "a")
    fWrite.write(algorithm + " (minimisation du " + evalType + ")")
    fWrite.write("\n\tAlgorithme : " + algorithm)
    fWrite.write("\n\tSeed : " + dataSplit[2][:-1])
    fWrite.write("\n\tGeneration : " + tmpData.split(' ')[0])
    fWrite.write("\n\tFitness " + "(" + evalType + ") : " + dataSplit[0])
    fWrite.write("\n\tSolution : " + tmpData.split("  ")[1])
    fWrite.write("\n\n")
    fWrite.close()

def exportPvalue(pvalue1, pvalue2):
    fWrite = open(resultFileName, "a")
    fWrite.write("\nP-value entre l\'algorithme cmaES et DE (Minimisation du temps) : " + str(pvalue1))
    fWrite.write("\nP-value entre l\'algorithme cmaES et DE (Minimisation du co2) : " + str(pvalue2))
    fWrite.close()

def compute(folderName):
    _files = []
    for path, subdirs, files in os.walk(folderName):
        for name in files:
            _files.append(os.path.join(path, name))
    fitnessList1ForCMaES = []
    fitnessList2ForCMaES = []
    fitnessList1ForDE = []
    fitnessList2ForDE = []
    for file in _files:
        f = open(file)
        fileDetails = file.split('/')[3].split('_')
        algorithm = fileDetails[1]
        evalType = fileDetails[0]
        data = f.readlines()
        bestData = data[0]
        del data[0]
        f.close()
        fitnessXList = []
        fitnessYList = []
        subPlotXList = []
        subPlotYList = []
        nbGen = 1
        for dataLine in data:
            fitnessXList.append(nbGen)
            tmpData = dataLine.split(',')
            fitnessYList.append(int(tmpData[0]))
            if(algorithm == 'cmaES'):
                if(evalType == '1'):
                    fitnessList1ForCMaES.append(int(tmpData[0]))
                else:
                    fitnessList2ForCMaES.append(int(tmpData[0]))
            else:
                if(evalType == '1'):
                    fitnessList1ForDE.append(int(tmpData[0]))
                else:
                    fitnessList2ForDE.append(int(tmpData[0]))
            dataLine = tmpData[1]
            needBox = nbGen % 10
            if needBox == 0 or nbGen == 1:
                subPlotXList.append(nbGen)
                subPlotYList.append(list(map(int, dataLine.split(' ')[:-1])))
            nbGen += 1
        tmpInfoList = file.split('_')
        algorithm = tmpInfoList[1]
        yTicks = []
        if (evalType == "1"):
            evalTypeView = 'temps'
            color = 'b'
            yTicks = timeTicks
        else:
            evalTypeView = 'co2'
            color = 'r'
            yTicks = co2Ticks
        plt3.plot(fitnessXList, fitnessYList, color, label=evalTypeView)
        plt3.boxplot(subPlotYList, positions=subPlotXList, widths=2)
        plt3.legend(loc='upper center')
        plt3.xlabel('Generation')
        plt3.ylabel(evalTypeView, color=color)
        plt3.xticks(subPlotXList)
        plt3.yticks(yTicks)
        plt3.subplots_adjust(left=0.15, right=0.85)
        plt3.title('Fitness moyenne pour l\'algorithme ' + algorithm + ' (minimisation du ' + evalTypeView + ')', y=1.05)
        plt3.savefig('results/so/' + file.split('/')[3].split('.')[0] + '.png', dpi=300)
        plt3.clf()
        exportResult(algorithm, evalTypeView, bestData)
    _, pvalue1 = stats.mannwhitneyu(fitnessList1ForCMaES, fitnessList1ForDE)
    _, pvalue2 = stats.mannwhitneyu(fitnessList2ForCMaES, fitnessList2ForDE)
    exportPvalue(pvalue1, pvalue2)

def main():
    if os.path.exists(resultFileName):
        os.remove(resultFileName)
    compute("results/data/so")
main()