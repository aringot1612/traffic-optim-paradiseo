/**
 * @file Base.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_BASE_HPP
#define TP4_BASE_HPP

#include <artis-star/common/time/DoubleTime.hpp>
#include <artis-star/common/Bag.hpp>
#include <artis-star/common/ExternalEvent.hpp>
#include <artis-star/common/observer/View.hpp>

namespace tp4 {

    typedef typename artis::common::DoubleTime::type Time;
    typedef typename artis::common::Bag<artis::common::DoubleTime> Bag;
    typedef typename artis::common::ExternalEvent<artis::common::DoubleTime> ExternalEvent;

    typedef artis::observer::View<artis::common::DoubleTime> View;

    typedef artis::common::Trace<artis::common::DoubleTime> Trace;
    typedef artis::common::TraceElement<artis::common::DoubleTime> TraceElement;

}

#endif