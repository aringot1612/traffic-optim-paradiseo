/**
 * @file LinkQueue.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LinkQueue.hpp"

#include <cmath>
#include <iostream>

namespace tp4 {

    void LinkQueue::dconf(const Time &t,
                          const Time &/* e */,
                          const Bag &bag) {
      dint(t);
      dext(t, 0, bag);
    }

    void LinkQueue::dint(const Time &t) {
      update_state(t);
      _last_time = t;
    }

    void LinkQueue::dext(const Time &t,
                         const Time &e,
                         const Bag &bag) {
      if (_sigma != artis::common::DoubleTime::infinity) {
        _sigma -= e;
      }
      std::for_each(bag.begin(), bag.end(),
                    [t, this](const ExternalEvent &event) {
                        if (event.on_port(inputs::IN)) {
                          Vehicle vehicle{};

                          event.data()(vehicle);
                          if (_vehicles.empty()) {
                            _vehicles.push_back(Entry{vehicle, t, t});
                          } else if (!_vehicles.empty()) {
                            _vehicles.push_back(Entry{vehicle, t,
                                                      std::max(t, _vehicles.back().next_time +
                                                                  _vehicles.back().vehicle.length / _free_speed)});
                          }
                          update_sigma(t);
                        } else if (event.on_port(inputs::IN_OPEN)) {
                          unsigned int index;

                          event.data()(index);
                          _open_links[index] = artis::common::DoubleTime::infinity;
                          if (!_vehicles.empty()) {
                            Vehicle &vehicle = _vehicles.front().vehicle;
                            unsigned int out_index =
                                (vehicle.path & (1 << (vehicle.path_index + 1))) >> (vehicle.path_index + 1);
                            if (_fully_close_links[out_index] && t > _vehicles.front().next_time) {
                              Time delay = t - _vehicles.front().next_time;
                              for (auto &v: _vehicles) {
                                v.next_time += delay;
                              }
                              _vehicles.front().next_time = t;
                            }
                          }
                          _fully_close_links[index] = false;
                          update_sigma(t);
                        } else if (event.on_port(inputs::IN_CLOSE)) {
                          Data data{};

                          event.data()(data);
                          _open_links[data.index] = data.open_time;
                          if (data.open_time == artis::common::DoubleTime::infinity) {
                            _fully_close_links[data.index] = true;
                          }
                        } else if (event.on_port(inputs::BACK)) {
                          Vehicle vehicle{};

                          event.data()(vehicle);
                          vehicle.path_index--;
                          _vehicles.push_front(Entry{vehicle, t, t});
                          /*for (auto &v: _vehicles) {
                              v.next_time += 0.1;
                          }*/
                          update_sigma(t);
                        }
                    });
      _last_time = t;
    }

    void LinkQueue::start(const Time &t) {
      _phase = Phase::WAIT;
      _sigma = artis::common::DoubleTime::infinity;
      _last_time = t;
    }

    Time LinkQueue::ta(const Time & /* t */) const {
      return _sigma;
    }

    Bag LinkQueue::lambda(const Time &t) const {
      Bag bag;

      if (_phase == Phase::SEND) {
        Vehicle vehicle = _vehicles.front().vehicle;

        vehicle.consumption += 2 * (t - _vehicles.front().in_time) * 2.4;
        bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT, vehicle));
      }
      return bag;
    }

    artis::common::Value LinkQueue::observe(const Time & /* t */,
                                            unsigned int index) const {
      switch (index) {
        case vars::VEHICLE_NUMBER:
          return (unsigned int) _vehicles.size();
        case vars::WAITING_DURATION_SUM:
          return (double) _waiting_duration_sum;
        default:
          return artis::common::Value();
      }
    }

    void LinkQueue::update_state(const Time &t) {
      if (_phase == Phase::WAIT and vehicle_ready(t)) {
        if (not _open_links.empty()) {
          Vehicle &vehicle = _vehicles.front().vehicle;
          unsigned int out_index = (vehicle.path & (1 << (vehicle.path_index + 1))) >> (vehicle.path_index + 1);

          // if next link is closed for undefined time
          if (_fully_close_links[out_index]) {
            _sigma = artis::common::DoubleTime::infinity;
          } else {
            // if the next link is closed then wait for link to open
            if ((_open_links[out_index] != artis::common::DoubleTime::infinity and t <= _open_links[out_index])) {
              double delay = _open_links[out_index] - t;
              for (auto &v: _vehicles) {
                v.next_time += delay;
              }
              update_sigma(t);
            } else { // else send vehicle to node (and next link)
              _phase = Phase::SEND;
              _vehicles.front().vehicle.path_index++;
              _sigma = 0;
            }
          }
        } else { // send vehicle to node (and next link)
          _phase = Phase::SEND;
          _vehicles.front().vehicle.path_index++;
          _sigma = 0;
        }
      } else if (_phase == Phase::SEND) {
        _phase = Phase::WAIT;

        _waiting_duration_sum += t - _vehicles.front().in_time;

        _vehicles.pop_front();
        update_sigma(t);
      }
    }

    void LinkQueue::update_sigma(const Time &t) {
      if (_vehicles.empty()) {
        _sigma = artis::common::DoubleTime::infinity;
      } else if (!_fully_close_links.empty()) {
        const Vehicle &vehicle = _vehicles.front().vehicle;
        unsigned int out_index = (vehicle.path & (1 << (vehicle.path_index + 1))) >> (vehicle.path_index + 1);
        if (_fully_close_links[out_index]) {
          _sigma = artis::common::DoubleTime::infinity;
        } else {
          _sigma = _vehicles.front().next_time - t;
        }
      } else {
        _sigma = _vehicles.front().next_time - t;
      }
    }

    bool LinkQueue::vehicle_ready(const Time &t) const {
      return (not _vehicles.empty() and std::abs(_vehicles.front().next_time - t) < 1e-6);
    }

}