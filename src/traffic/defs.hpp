/**
 * @file def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP_ARTIS_DEF_HPP
#define TP_ARTIS_DEF_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include "LinkQueue.hpp"
#include "LinkTravel.hpp"
#include "Node.hpp"
#include "Counter.hpp"
#include "Generator.hpp"

struct LinkParameters {
    double length;
    unsigned int lane_number;
    double free_speed;
    double concentration;
    double wave_speed;
    double capacity;
    unsigned int out_link_number;
    int id_link;
};

class LinkGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, LinkParameters> {
public:
    enum submodels {
        TRAVEL,
        QUEUE
    };

    struct inputs {
        enum values {
            IN, IN_CONFIRM_OUT, IN_OPEN, IN_CLOSE, BACK
        };
    };

    struct outputs {
        enum values {
            OUT, OUT_OPEN, OUT_CLOSE
        };
    };

    LinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                     const LinkParameters &parameters,
                     const artis::common::NoParameters &graph_parameters)
        :
        artis::pdevs::GraphManager<artis::common::DoubleTime,
            LinkParameters>(
            coordinator, parameters, graph_parameters),
        _queue("queue", {parameters.length,
                         parameters.lane_number,
                         parameters.free_speed,
                         parameters.concentration,
                         parameters.wave_speed,
                         parameters.capacity,
                         parameters.out_link_number,
                         parameters.id_link}),
        _travel("travel", {parameters.length,
                           parameters.lane_number,
                           parameters.free_speed,
                           parameters.concentration,
                           parameters.wave_speed,
                           parameters.capacity,
                           {},
                           parameters.id_link}) {
      add_child(QUEUE, &_queue);
      add_child(TRAVEL, &_travel);

      coordinator->input_ports({{inputs::IN,             "in"},
                                {inputs::IN_CONFIRM_OUT, "in_confirm_out"},
                                {inputs::IN_OPEN,        "in_open"},
                                {inputs::IN_CLOSE,       "in_close"},
                                {inputs::BACK,           "back"}});
      coordinator->output_ports({
                                    {outputs::OUT,       "out"},
                                    {outputs::OUT_OPEN,  "out_open"},
                                    {outputs::OUT_CLOSE, "out_close"}});

      in({coordinator, inputs::IN})
          >> in({&_travel, tp4::LinkTravel::inputs::IN});
      in({coordinator, inputs::IN_CONFIRM_OUT})
          >> in({&_travel, tp4::LinkTravel::inputs::IN_CONFIRM_OUT});
      in({coordinator, inputs::IN_OPEN})
          >> in({&_queue, tp4::LinkQueue::inputs::IN_OPEN});
      in({coordinator, inputs::IN_CLOSE})
          >> in({&_queue, tp4::LinkQueue::inputs::IN_CLOSE});
      in({coordinator, inputs::BACK})
          >> in({&_queue, tp4::LinkQueue::inputs::BACK});
      out({&_travel, tp4::LinkTravel::outputs::OUT})
          >> in({&_queue, tp4::LinkQueue::inputs::IN});
      out({&_travel, tp4::LinkTravel::outputs::OUT_OPEN})
          >> out({coordinator, outputs::OUT_OPEN});
      out({&_travel, tp4::LinkTravel::outputs::OUT_CLOSE})
          >> out({coordinator, outputs::OUT_CLOSE});
      out({&_queue, tp4::LinkQueue::outputs::OUT})
          >> out({coordinator, outputs::OUT});
    }

    ~LinkGraphManager() override = default;

private:
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::LinkQueue,
        tp4::LinkQueueParameters> _queue;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::LinkTravel,
        tp4::LinkTravelParameters> _travel;
};

struct SimpleNetworkParameters {
    tp4::GeneratorParameters generator_1_parameters;
    tp4::GeneratorParameters generator_8_parameters;
    tp4::GeneratorParameters generator_11_parameters;
    LinkParameters link_1_parameters;
    LinkParameters link_2_parameters;
    LinkParameters link_3_parameters;
    LinkParameters link_4_parameters;
    LinkParameters link_5_parameters;
    LinkParameters link_6_parameters;
    LinkParameters link_7_parameters;
    LinkParameters link_8_parameters;
    LinkParameters link_9_parameters;
    LinkParameters link_10_parameters;
    LinkParameters link_11_parameters;
    LinkParameters link_12_parameters;
    tp4::NodeParameters node_3_parameters;
    tp4::NodeParameters node_4_parameters;
    tp4::NodeParameters node_5_parameters;
    tp4::NodeParameters node_6_parameters;
    tp4::NodeParameters node_9_parameters;
    tp4::NodeParameters node_10_parameters;
};

class SimpleNetworkGraphManager :
    public artis::pdevs::GraphManager<artis::common::DoubleTime, SimpleNetworkParameters> {
public:
    enum submodels {
        GENERATOR_1,
        GENERATOR_8,
        GENERATOR_11,
        LINK_1,
        LINK_2,
        LINK_3,
        LINK_4,
        LINK_5,
        LINK_6,
        LINK_7,
        LINK_8,
        LINK_9,
        LINK_10,
        LINK_11,
        LINK_12,
        NODE_3,
        NODE_4,
        NODE_5,
        NODE_6,
        NODE_9,
        NODE_10,
        COUNTER_2,
        COUNTER_4,
        COUNTER_7,
        COUNTER_12
    };

    SimpleNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                              const SimpleNetworkParameters &parameters,
                              const artis::common::NoParameters &graph_parameters)
        :
        artis::pdevs::GraphManager<artis::common::DoubleTime, SimpleNetworkParameters>(
            coordinator, parameters, graph_parameters),
        _generator_1("generator_1", parameters.generator_1_parameters),
        _generator_8("generator_8", parameters.generator_8_parameters),
        _generator_11("generator_11", parameters.generator_11_parameters),
        _link_1("L1", parameters.link_1_parameters, graph_parameters),
        _link_2("L2", parameters.link_2_parameters, graph_parameters),
        _link_3("L3", parameters.link_3_parameters, graph_parameters),
        _link_4("L4", parameters.link_4_parameters, graph_parameters),
        _link_5("L5", parameters.link_5_parameters, graph_parameters),
        _link_6("L6", parameters.link_6_parameters, graph_parameters),
        _link_7("L7", parameters.link_7_parameters, graph_parameters),
        _link_8("L8", parameters.link_8_parameters, graph_parameters),
        _link_9("L9", parameters.link_9_parameters, graph_parameters),
        _link_10("L10", parameters.link_10_parameters, graph_parameters),
        _link_11("L11", parameters.link_11_parameters, graph_parameters),
        _link_12("L12", parameters.link_12_parameters, graph_parameters),
        _node_3("node_3", parameters.node_3_parameters),
        _node_4("node_4", parameters.node_4_parameters),
        _node_5("node_5", parameters.node_5_parameters),
        _node_6("node_6", parameters.node_6_parameters),
        _node_9("node_9", parameters.node_9_parameters),
        _node_10("node_10", parameters.node_10_parameters),
        _counter_2("counter_2", artis::common::NoParameters()),
        _counter_4("counter_4", artis::common::NoParameters()),
        _counter_7("counter_7", artis::common::NoParameters()),
        _counter_12("counter_12", artis::common::NoParameters()) {
      add_child(GENERATOR_1, &_generator_1);
      add_child(GENERATOR_8, &_generator_8);
      add_child(GENERATOR_11, &_generator_11);
      add_child(LINK_1, &_link_1);
      add_child(LINK_2, &_link_2);
      add_child(LINK_3, &_link_3);
      add_child(LINK_4, &_link_4);
      add_child(LINK_5, &_link_5);
      add_child(LINK_6, &_link_6);
      add_child(LINK_7, &_link_7);
      add_child(LINK_8, &_link_8);
      add_child(LINK_9, &_link_9);
      add_child(LINK_10, &_link_10);
      add_child(LINK_11, &_link_11);
      add_child(LINK_12, &_link_12);
      add_child(NODE_3, &_node_3);
      add_child(NODE_4, &_node_4);
      add_child(NODE_5, &_node_5);
      add_child(NODE_6, &_node_6);
      add_child(NODE_9, &_node_9);
      add_child(NODE_10, &_node_10);
      add_child(COUNTER_2, &_counter_2);
      add_child(COUNTER_4, &_counter_4);
      add_child(COUNTER_7, &_counter_7);
      add_child(COUNTER_12, &_counter_12);

      out({&_generator_11, tp4::Generator::outputs::OUT})
          >> in({&_link_1, LinkGraphManager::inputs::IN});
      out({&_link_1, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_generator_11, tp4::Generator::inputs::IN_OPEN});
      out({&_link_1, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_generator_11, tp4::Generator::inputs::IN_CLOSE});
      out({&_link_1, LinkGraphManager::outputs::OUT})
          >> in({&_node_9, tp4::Node::inputs::IN});
      out({&_node_9, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_1, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_9, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_1, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_9, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_1, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_9, tp4::Node::outputs::OUT})
          >> in({&_link_2, LinkGraphManager::inputs::IN});
      out({&_link_2, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_9, tp4::Node::inputs::IN_OPEN});
      out({&_link_2, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_9, tp4::Node::inputs::IN_CLOSE});
      out({&_link_2, LinkGraphManager::outputs::OUT})
          >> in({&_node_5, tp4::Node::inputs::IN});
      out({&_node_5, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_2, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_5, tp4::Node::outputs::BACK})
          >> in({&_link_2, LinkGraphManager::inputs::BACK});
      out({&_node_5, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_2, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_5, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_2, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_5, tp4::Node::outputs::OUT})
          >> in({&_link_7, LinkGraphManager::inputs::IN});
      out({&_link_7, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_5, tp4::Node::inputs::IN_OPEN});
      out({&_link_7, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_5, tp4::Node::inputs::IN_CLOSE});
      out({&_link_7, LinkGraphManager::outputs::OUT})
          >> in({&_counter_2, tp4::Node::inputs::IN});
      out({&_counter_2, tp4::Counter::outputs::OUT_CONFIRM})
          >> in({&_link_7, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_9, tp4::Node::outputs::OUT + 1})
          >> in({&_link_3, LinkGraphManager::inputs::IN});
      out({&_link_3, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_9, tp4::Node::inputs::IN_OPEN + 1});
      out({&_link_3, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_9, tp4::Node::inputs::IN_CLOSE + 1});
      out({&_link_3, LinkGraphManager::outputs::OUT})
          >> in({&_counter_4, tp4::Counter::inputs::IN});
      out({&_counter_4, tp4::Counter::outputs::OUT_CONFIRM})
          >> in({&_link_3, LinkGraphManager::inputs::IN_CONFIRM_OUT});

      out({&_generator_8, tp4::Generator::outputs::OUT})
          >> in({&_link_4, LinkGraphManager::inputs::IN});
      out({&_link_4, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_generator_8, tp4::Generator::inputs::IN_OPEN});
      out({&_link_4, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_generator_8, tp4::Generator::inputs::IN_CLOSE});
      out({&_link_4, LinkGraphManager::outputs::OUT})
          >> in({&_node_6, tp4::Node::inputs::IN});
      out({&_node_6, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_4, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_6, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_4, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_6, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_4, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_6, tp4::Node::outputs::OUT})
          >> in({&_link_6, LinkGraphManager::inputs::IN});
      out({&_link_6, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_6, tp4::Node::inputs::IN_OPEN});
      out({&_link_6, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_6, tp4::Node::inputs::IN_CLOSE});
      out({&_link_6, LinkGraphManager::outputs::OUT})
          >> in({&_node_5, tp4::Node::inputs::IN + 1});
      out({&_link_5, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_6, tp4::Node::inputs::IN_OPEN + 1});
      out({&_node_5, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_6, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_5, tp4::Node::outputs::BACK + 1})
          >> in({&_link_6, LinkGraphManager::inputs::BACK});
      out({&_node_5, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_6, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_5, tp4::Node::outputs::OUT_CONFIRM + 1})
          >> in({&_link_6, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_link_5, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_6, tp4::Node::inputs::IN_CLOSE + 1});
      out({&_node_6, tp4::Node::outputs::OUT + 1})
          >> in({&_link_5, LinkGraphManager::inputs::IN});
      out({&_link_5, LinkGraphManager::outputs::OUT})
          >> in({&_node_10, tp4::Node::inputs::IN});
      out({&_node_10, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_5, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_10, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_5, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_10, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_5, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_10, tp4::Node::outputs::BACK})
          >> in({&_link_5, LinkGraphManager::inputs::BACK});
      out({&_node_10, tp4::Node::outputs::OUT})
          >> in({&_link_10, LinkGraphManager::inputs::IN});
      out({&_link_10, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_10, tp4::Node::inputs::IN_OPEN});
      out({&_link_10, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_10, tp4::Node::inputs::IN_CLOSE});
      out({&_link_10, LinkGraphManager::outputs::OUT})
          >> in({&_counter_12, tp4::Counter::inputs::IN});
      out({&_counter_12, tp4::Counter::outputs::OUT_CONFIRM})
          >> in({&_link_10, LinkGraphManager::inputs::IN_CONFIRM_OUT});

      out({&_generator_1, tp4::Generator::outputs::OUT})
          >> in({&_link_8, LinkGraphManager::inputs::IN});
      out({&_link_8, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_generator_1, tp4::Generator::inputs::IN_OPEN});
      out({&_link_8, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_generator_1, tp4::Generator::inputs::IN_CLOSE});
      out({&_link_8, LinkGraphManager::outputs::OUT})
          >> in({&_node_3, tp4::Node::inputs::IN});
      out({&_node_3, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_8, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_3, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_8, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_3, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_8, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_3, tp4::Node::outputs::OUT})
          >> in({&_link_9, LinkGraphManager::inputs::IN});
      out({&_link_9, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_3, tp4::Node::inputs::IN_OPEN});
      out({&_link_9, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_3, tp4::Node::inputs::IN_CLOSE});
      out({&_link_9, LinkGraphManager::outputs::OUT})
          >> in({&_node_10, tp4::Node::inputs::IN + 1});
      out({&_node_10, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_9, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_10, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_9, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_10, tp4::Node::outputs::OUT_CONFIRM + 1})
          >> in({&_link_9, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_10, tp4::Node::outputs::BACK + 1})
          >> in({&_link_9, LinkGraphManager::inputs::BACK});
      out({&_node_3, tp4::Node::outputs::OUT + 1})
          >> in({&_link_11, LinkGraphManager::inputs::IN});
      out({&_link_11, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_3, tp4::Node::inputs::IN_OPEN + 1});
      out({&_link_11, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_3, tp4::Node::inputs::IN_CLOSE + 1});
      out({&_link_11, LinkGraphManager::outputs::OUT})
          >> in({&_node_4, tp4::Node::inputs::IN});
      out({&_node_4, tp4::Node::outputs::OUT_CLOSE})
          >> in({&_link_11, LinkGraphManager::inputs::IN_CLOSE});
      out({&_node_4, tp4::Node::outputs::OUT_OPEN})
          >> in({&_link_11, LinkGraphManager::inputs::IN_OPEN});
      out({&_node_4, tp4::Node::outputs::OUT_CONFIRM})
          >> in({&_link_11, LinkGraphManager::inputs::IN_CONFIRM_OUT});
      out({&_node_4, tp4::Node::outputs::OUT})
          >> in({&_link_12, LinkGraphManager::inputs::IN});
      out({&_link_12, LinkGraphManager::outputs::OUT_OPEN})
          >> in({&_node_4, tp4::Node::inputs::IN_OPEN});
      out({&_link_12, LinkGraphManager::outputs::OUT_CLOSE})
          >> in({&_node_4, tp4::Node::inputs::IN_CLOSE});
      out({&_link_12, LinkGraphManager::outputs::OUT})
          >> in({&_counter_7, tp4::Counter::inputs::IN});
      out({&_counter_7, tp4::Counter::outputs::OUT_CONFIRM})
          >> in({&_link_12, LinkGraphManager::inputs::IN_CONFIRM_OUT});
    }

    ~SimpleNetworkGraphManager() override = default;

private:
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Generator,
        tp4::GeneratorParameters> _generator_1;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Generator,
        tp4::GeneratorParameters> _generator_8;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Generator,
        tp4::GeneratorParameters> _generator_11;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_1;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_2;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_3;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_4;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_5;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_6;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_7;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_8;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_9;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_10;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_11;
    artis::pdevs::Coordinator<artis::common::DoubleTime,
        LinkGraphManager,
        LinkParameters> _link_12;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_3;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_4;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_5;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_6;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_9;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Node,
        tp4::NodeParameters> _node_10;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Counter> _counter_2;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Counter> _counter_4;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Counter> _counter_7;
    artis::pdevs::Simulator<artis::common::DoubleTime,
        tp4::Counter> _counter_12;
};

class TwoLinkWithNodeView : public artis::observer::View<artis::common::DoubleTime> {
public:
    TwoLinkWithNodeView() {
      selector("Generator_1:counter",
               {SimpleNetworkGraphManager::GENERATOR_1,
                tp4::Generator::vars::COUNTER});
      selector("Generator_8:counter",
               {SimpleNetworkGraphManager::GENERATOR_8,
                tp4::Generator::vars::COUNTER});
      selector("Generator_11:counter",
               {SimpleNetworkGraphManager::GENERATOR_11,
                tp4::Generator::vars::COUNTER});
      selector("Link_1:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_1,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_2:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_2,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_3:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_3,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_4:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_4,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_5:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_5,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_6:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_6,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_7:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_7,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_8:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_8,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_9:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_9,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_10:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_10,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_11:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_11,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Link_12:waiting_duration_sum",
               {SimpleNetworkGraphManager::LINK_12,
                LinkGraphManager::QUEUE,
                tp4::LinkQueue::vars::WAITING_DURATION_SUM});
      selector("Counter_2:counter",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::COUNTER});
      selector("Counter_4:counter",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::COUNTER});
      selector("Counter_7:counter",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::COUNTER});
      selector("Counter_12:counter",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::COUNTER});
      selector("Counter_2:travel_duration_sum",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::TRAVEL_DURATION_SUM});
      selector("Counter_4:travel_duration_sum",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::TRAVEL_DURATION_SUM});
      selector("Counter_7:travel_duration_sum",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::TRAVEL_DURATION_SUM});
      selector("Counter_12:travel_duration_sum",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::TRAVEL_DURATION_SUM});
      selector("Counter_2:travel_duration_min",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::TRAVEL_DURATION_MIN});
      selector("Counter_4:travel_duration_min",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::TRAVEL_DURATION_MIN});
      selector("Counter_7:travel_duration_min",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::TRAVEL_DURATION_MIN});
      selector("Counter_12:travel_duration_min",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::TRAVEL_DURATION_MIN});
      selector("Counter_2:travel_duration_max",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::TRAVEL_DURATION_MAX});
      selector("Counter_4:travel_duration_max",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::TRAVEL_DURATION_MAX});
      selector("Counter_7:travel_duration_max",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::TRAVEL_DURATION_MAX});
      selector("Counter_12:travel_duration_max",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::TRAVEL_DURATION_MAX});
      selector("Counter_2:consumption_sum",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::CONSUMPTION_SUM});
      selector("Counter_4:consumption_sum",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::CONSUMPTION_SUM});
      selector("Counter_7:consumption_sum",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::CONSUMPTION_SUM});
      selector("Counter_12:consumption_sum",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::CONSUMPTION_SUM});
      selector("Counter_2:consumption_min",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::CONSUMPTION_MIN});
      selector("Counter_4:consumption_min",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::CONSUMPTION_MIN});
      selector("Counter_7:consumption_min",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::CONSUMPTION_MIN});
      selector("Counter_12:consumption_min",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::CONSUMPTION_MIN});
      selector("Counter_2:consumption_max",
               {SimpleNetworkGraphManager::COUNTER_2,
                tp4::Counter::vars::CONSUMPTION_MAX});
      selector("Counter_4:consumption_max",
               {SimpleNetworkGraphManager::COUNTER_4,
                tp4::Counter::vars::CONSUMPTION_MAX});
      selector("Counter_7:consumption_max",
               {SimpleNetworkGraphManager::COUNTER_7,
                tp4::Counter::vars::CONSUMPTION_MAX});
      selector("Counter_12:consumption_max",
               {SimpleNetworkGraphManager::COUNTER_12,
                tp4::Counter::vars::CONSUMPTION_MAX});
    }
};

#endif
