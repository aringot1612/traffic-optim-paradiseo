#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>
#include <set>
#include <eo>
#include "omp.h"
#include "../utils/vectorUtility.h"

namespace fs = std::filesystem;

int main(int argc, char** argv) {
  std::vector<std::string> globalPaths {"../output/1/", "../output/2/"};
  std::vector<std::string> algorithms {"cmaES", "DE"};
  #pragma omp parallel for
  for(unsigned int pathIndex = 0 ; pathIndex < globalPaths.size() ; pathIndex++){
    #pragma omp parallel for
    for(unsigned int algorithmIndex = 0 ; algorithmIndex < algorithms.size() ; algorithmIndex++){
        int bestFitness = 9999999;
        std::string bestSeed;
        std::string bestLine;
        int globalIndex = pathIndex;
        if(globalIndex == 0)
          globalIndex += algorithmIndex;
        else
          globalIndex = (globalIndex*2) + algorithmIndex;
        std::vector<std::string> globalPathDetails = VectorUtility::split(globalPaths.at(pathIndex), '/');
        std::string algorithm = algorithms.at(algorithmIndex);
        std::vector<std::vector<int>> fitnessList(0);
        std::string line;
        int feedCount = 0;
        int goodGenCount = 999;
        int totalCount = std::distance(fs::directory_iterator(globalPaths.at(pathIndex)), fs::directory_iterator());
        for (const auto & entry : fs::directory_iterator(globalPaths.at(pathIndex))){
          // Pour chaque seed...
          feedCount++;
          for (const auto & entry : fs::directory_iterator(entry)){
            // Pour chaque fichier...
            std::vector<std::string> pathDetails = VectorUtility::split(entry.path(), '/');
            if(pathDetails.at(4) == algorithm + ".csv"){
              std::fstream file;
              file.open(entry.path(), std::ios::in | std::ios::out);
              if (file.is_open()){
                getline(file, line);
                int maxGen = 0;
                while(getline(file, line)){
                  std::vector<std::string> lineDetails = VectorUtility::split(line, ' ');
                  int nbGen = std::stoi(lineDetails[0]);
                  if(maxGen < nbGen)
                    maxGen = nbGen;
                  if(fitnessList.size() < nbGen){
                    fitnessList.push_back(std::vector<int>(0));
                  }
                  int tmpFitness = std::stoi(lineDetails[3]);
                  if(bestFitness > tmpFitness){
                    bestFitness = tmpFitness;
                    bestLine = line;
                    bestSeed = pathDetails[3];
                  }
                  fitnessList.at(nbGen-1).push_back(tmpFitness);
                }
                if(goodGenCount > maxGen){
                  goodGenCount = maxGen;
                }
              }
              file.close();
            }
          }
        }
        std::ofstream file("../results/data/so/" + globalPathDetails.at(2) + "_" + algorithm + "_" + ".sav");
        file << bestFitness << "," << bestLine << "," << bestSeed << std::endl;
        for(unsigned int i = 0 ; i < goodGenCount ; i++){
          file << VectorUtility::concatenateSpecial(fitnessList.at(i)) << std::endl;
        }
        file.close();
    }
  }
  return 0;
}