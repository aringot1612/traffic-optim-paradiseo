#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>
#include <set>
#include <eo>
#include "omp.h"
#include "../utils/moComponents/fileReader.h"
#include "../utils/moComponents/solution.h"
#include "../utils/moComponents/ndsfa.h"
#include "../utils/vectorUtility.h"
#include <climits>

namespace fs = std::filesystem;

void computeParetoFront(std::string path, FileReader* reader, Ndsfa* ndsfa, bool savePop){
  std::vector<std::string> pathDetails = VectorUtility::split(path, '/');
  std::vector<std::vector<int>> fitnessList = reader->readFile(path);
  unsigned long long hypervolume = 0;
  std::vector<Solution> solutions;
  std::vector<int> timeFitnessList(fitnessList.size());
  std::vector<int> co2FitnessList(fitnessList.size());
  for(unsigned int i = 0 ; i < fitnessList.size() ; i++){
    solutions.push_back(Solution(fitnessList.at(i), i));
    timeFitnessList[i] = fitnessList.at(i).at(0);
    co2FitnessList[i] = fitnessList.at(i).at(1);
  }
  std::vector<Solution> nonDominatedSolutions = ndsfa->findNonDominatedSolutions(solutions);
  std::vector<int> timeFitnessListNonDominated(nonDominatedSolutions.size());
  std::vector<int> co2FitnessListNonDominated(nonDominatedSolutions.size());
  for(unsigned int i = 0 ; i < nonDominatedSolutions.size() ; i++){
    timeFitnessListNonDominated[i] = nonDominatedSolutions.at(i).getFitness(0);
    co2FitnessListNonDominated[i] = nonDominatedSolutions.at(i).getFitness(1);
  }
  if(savePop){
    std::ofstream file("../results/data/mo/algorithms/" + pathDetails.at(3) + "_" + pathDetails.at(5));
    file << VectorUtility::concatenateInt(timeFitnessList) << std::endl;
    file << VectorUtility::concatenateInt(co2FitnessList) << std::endl;
    file << VectorUtility::concatenateInt(timeFitnessListNonDominated) << std::endl;
    file << VectorUtility::concatenateInt(co2FitnessListNonDominated) << std::endl;
    file.close();
  }
}

int main(int argc, char** argv) {
  FileReader reader;
  Ndsfa ndsfa;
  std::ifstream file ("../results/mo/bestHyperVolumes.txt");
  std::string line;
  int lineIndex = 0;
  if (file.is_open())
    while(getline(file, line)){
      if((lineIndex == 2 || lineIndex == 5 || lineIndex == 8) && (!line.empty())){
        computeParetoFront(line, &reader, &ndsfa, true);
      }
      lineIndex++;
    }
  file.close();
  return 0;
}