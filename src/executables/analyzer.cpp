#include <vector>
#include <iostream>
#include <cstdlib>
#include "../utils/simulator.h"

/** Executable permettant de connaître la fitness détaillée d'une solution en fournissant les paramètres. */
int main(int argc, char** argv) {
  std::vector<double> values;
  for(unsigned int i = 0 ; i < argc ; i++){
    values.push_back(atof(argv[i]));
  }
  values.erase(values.begin());
  std::vector<double> result = Simulator<std::vector<double>>::evaluate(values);
  std::cout << "Results -->  Time :  "<< result[1] << " CO2 : " << result[2] << " Vehicles : " << result[3] << ";\n" << std::endl;
}