#include <eo>
#include <es/eoRealInitBounded.h>
#include <es/eoRealOp.h>
#include <moeo>
#include <DTLZ/src/PolynomialMutation.h>
#include <DTLZ/src/SBXCrossover.h>

// Fonction d'évaluation.
#include "../utils/evals/trafficEvalMO.h"

// Bornes solution.
#include "../utils/bounds.h"

// Type du vecteur objectif, stocke, entre autres, les fitness cibles à minimiser.
typedef moeoRealObjectiveVector<moeoObjectiveVectorTraits> ObjectiveVector;

// Type solution.
typedef moeoRealVector<ObjectiveVector> Indi;

/** Algorithme IBEA. */
int main(int argc, char** argv) {
  /* =========================================================
    *
    * Parameters
    *
    * ========================================================= */
  eoParser parser(argc, argv);
  // Seed.
  uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();
  // Taille d'une solution.
  size_t d = 24;
  // Taille de population.
  uint32_t mu = parser.createParam(50, "popSize", "Population size", 'P', "Algorithm").value();
  // mutation rate
  double mutation_rate = parser.createParam(0.95, "pmut", "Mutation rate", 'M', "Algorithm").value();
  // xover rate
  double xover_rate = parser.createParam(0.9, "xoverRate", "Crossover rate", 'X', "Algorithm").value();
  // mutation rate per variable
  double mutpervar_rate = parser.createParam(1.0, "vrate", "Mutation rate per variable", 'v', "Algorithm").value();
  // Paramètre eta pour la mutation polynomial (aussi appelée mutation GA).
  double mut_eta = parser.createParam(1.0, "meta", "eta parameter of polynomial mutation", 'm', "Algorithm").value();
  // xover eta
  double xover_eta = parser.createParam(1.0, "xeta", "eta parameter of SBX crossover", 'x', "Algorithm").value();
  // Facteur kappa.
  double kappa = parser.createParam(0.3, "kappa", "Kappa", 'K', "Algorithm").value();
  // Critère d'arrêt : le temps.
  time_t duration = parser.createParam(60, "time", "Time limit stopping criterium (number of seconds)", 'T', "Algorithm").value();
  // Fichier permettant de stocker l'ensemble des paramètres d'algorithme.
  std::string str_status = parser.ProgramName() + ".status";
  eoValueParam<std::string> statusParam(str_status.c_str(), "status", "Status file");
  parser.processParam( statusParam, "Persistence" );

  make_verbose(parser);
  make_help(parser);

  /* =========================================================
    *
    * Fonction d'évaluation avec vecteur de fitness à minimiser.
    *
    * ========================================================= */
  // Deux fitness à minimiser.
  std::vector<bool> objVec(2, true);
  moeoObjectiveVectorTraits::setup(2, objVec);
  // Fonction d'évaluation.
  TrafficEvalMO<Indi> _eval;
  eoPopLoopEval<Indi> popEval(_eval);
  eoEvalFuncCounter<Indi> eval(_eval, "neval");

  /* =========================================================
    *
    * Random seed.
    *
    * ========================================================= */
  rng.reseed(seed);

  /* =========================================================
    *
    * Initialisation.
    *
    * ========================================================= */
  // Initialisation de solutions avec bornes.
  eoRealInitBounded<Indi> init(Bounds::bounds);
  // Création de la population avec initialisation.
  eoPop<Indi> pop(mu, init);

  /* =========================================================
    *
    * Continuator: Critère d'arrêt.
    *
    * ========================================================= */
  eoTimeContinue<Indi> continuator(duration);

  /* =========================================================
    *
    * Opérateurs de variation.
    *
    * ========================================================= */
  PolynomialMutation<Indi> mutation(Bounds::bounds, mutpervar_rate, mut_eta);
  SBXCrossover<Indi> xover(Bounds::bounds, xover_eta);

  /* =========================================================
    *
    * Opérateurs moeo.
    *
    * ========================================================= */
  eoSGATransform<Indi> transform(xover, xover_rate, mutation, mutation_rate);
  moeoAdditiveEpsilonBinaryMetric<ObjectiveVector> indicator;

  /* =========================================================
    *
    * Sortie pour avoir l'avancement de l'algorithme.
    *
    * ========================================================= */
  eoCheckPoint<Indi> checkpoint(continuator);
  eoState outState;
  outState.registerObject(pop);
  eoCountedStateSaver stateSaver(1, outState, "../output/0/" + std::to_string(seed) + "/IBEA/generation_IBEA_", "dat");
  checkpoint.add(stateSaver);

  /* =========================================================
    *
    * L'algorithme.
    *
    * ========================================================= */
  moeoIBEA<Indi> solver(checkpoint, popEval, transform, indicator, kappa);

  /* =========================================================
    *
    * Lancement de l'algorithme.
    *
    * ========================================================= */
  for(unsigned i = 0; i < pop.size(); i++) {
    init(pop[i]);
    _eval(pop[i]);
  }
  solver(pop);
  pop.sort();
  return 0;
}