#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>
#include <set>
#include <eo>
#include "omp.h"
#include "../utils/moComponents/fileReader.h"
#include "../utils/moComponents/solution.h"
#include "../utils/moComponents/ndsfa.h"
#include "../utils/vectorUtility.h"
#include <climits>

namespace fs = std::filesystem;

void exportForR(std::string fileName, FileReader* reader){
  std::vector<std::string> pathDetails = VectorUtility::split(fileName, '_');
  std::vector<std::string> pathDetailsExtended = VectorUtility::split(pathDetails[2], '.');
  if(pathDetailsExtended[0].size() == 1)
    pathDetailsExtended[0].insert(pathDetailsExtended[0].begin(), '0');
  if(pathDetailsExtended[0].size() == 2)
    pathDetailsExtended[0].insert(pathDetailsExtended[0].begin(), '0');
  std::string newName = pathDetails.at(0) + '_' + pathDetails.at(1) + '_' + pathDetailsExtended.at(0) + '.' + pathDetailsExtended.at(1);
  rename(fileName.c_str(), newName.c_str());
}

int main(int argc, char** argv) {
  FileReader reader;
  for (const auto & entry : fs::directory_iterator("../output/0/")){
    for (const auto & entry : fs::directory_iterator(entry.path())){
      for (const auto & entry : fs::directory_iterator(entry.path())){
        exportForR(entry.path(), &reader);
      }
    }
  }
  return 0;
}