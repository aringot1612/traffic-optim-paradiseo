#include <eo>
#include <es.h>

// Fonction d'évaluation.
#include "../utils/evals/trafficEval.h"

// Composants de l'algorithme DE.
#include "../utils/deComponents/deBreed.h" // Gére la variation des solutions.
#include "../utils/deComponents/deRandMutation.h" // Gére la mutation des solutions.
#include "../utils/deComponents/deXover.h" // Gére le croisement des solutions.
#include "../utils/deComponents/deReplacement.h" // Gére le remplacement des solutions.

// Bornes solution.
#include "../utils/bounds.h"

// On cherche à minimiser une fitness.
typedef eoMinimizingFitness Fitness;

// Type solution.
typedef eoReal<Fitness> Indi;

/** Algorithme DE. */
int main(int argc, char** argv) {
  /* =========================================================
    *
    * Paramètres.
    *
    * ========================================================= */
  eoParser parser(argc, argv);
  // Seed.
  uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();
  // Taille d'une solution.
  size_t d = 24;
  // Taille de population.
  uint32_t mu = parser.createParam(50, "popSize", "Population size", 'P', "Algorithm").value();
  // Facteur de mutation.
  double mutationFactor = parser.createParam(0.8, "mutationFactor", "Mutation factor", 'f', "Algorithm").value();
  // Facteur de croisement.
  double xoverRate = parser.createParam(0.9, "xoverRate", "CrossOver rate (CR)", 'X', "Algorithm").value();
  // Critère d'arrêt : le temps.
  time_t duration = parser.createParam(60, "time", "Time limit stopping criterium (number of seconds)", 'T', "Algorithm").value();
  // Type d'évaluation (1 -> minimisation du temps | 2 -> minimisation du co2).
  int evalType = parser.createParam(1, "evalType", "Type d'évaluation : minimisation temps (1) ou co2 (2)", 'F', "Algorithm").value();
  // Fichier de sortie pour l'avancement de l'algorithme.
  std::string fileOutName = parser.createParam(std::string("../output/" + std::to_string(evalType) + "/" + std::to_string(seed) + "/" +  + "DE.csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();
  // Fichier permettant de stocker l'ensemble des paramètres d'algorithme.
  std::string str_status = parser.ProgramName() + ".status";
  eoValueParam<std::string> statusParam(str_status.c_str(), "status", "Status file");
  parser.processParam( statusParam, "Persistence" );

  make_verbose(parser);
  make_help(parser);

  /* =========================================================
    *
    * Fonction d'évaluation.
    *
    * ========================================================= */
  TrafficEval<Indi> _eval(evalType);
  // Permet de compter le nombre d'évaluation.
  eoEvalFuncCounter<Indi> eval(_eval, "neval");

  /* =========================================================
    *
    * Random seed.
    *
    * ========================================================= */
  rng.reseed(seed);

  /* =========================================================
    *
    * Initialisation
    *
    * ========================================================= */
  eoRealInitBounded<Indi> init(Bounds::bounds);

  /* =========================================================
    *
    * Continuator: Critère d'arrêt.
    *
    * ========================================================= */
  eoTimeContinue<Indi> continuator(duration);

  /* =========================================================
    *
    * Opérateurs de variation.
    *
    * ========================================================= */
  // Création d'une population.
  eoPop<Indi> pop;
  pop.resize(mu);
  // mutation DE. 
  DeRandMutation<Indi> mutation(pop, mutationFactor, Bounds::bounds);
  // croisement DE. 
  DEXover<Indi> xover(pop, xoverRate);
  // variation DE (utilise à la fois la mutation et le croisement pour créer des enfants).
  DEBreed<Indi> breed(mutation, xover);

  /* =========================================================
    *
    * Remplacement.
    *
    * ========================================================= */
  DEReplacement<Indi> replace;

  /* =========================================================
    *
    * Sortie pour avoir l'avancement de l'algorithme.
    *
    * ========================================================= */
  eoCheckPoint<Indi> checkpoint(continuator);
  eoFileMonitor monitor(fileOutName, " ", false, true);
  checkpoint.add(monitor);
  eoValueParam<unsigned> generationCounter(0, "iteration");
  eoIncrementor<unsigned> increment(generationCounter.value());
  checkpoint.add(increment);
  eoTimeCounter timeStat;
  checkpoint.add(timeStat);
  eoBestFitnessStat<Indi> bestStat("best");
  checkpoint.add(bestStat);
  eoSecondMomentStats<Indi> avgStdStat("avg std");
  checkpoint.add(avgStdStat);
  eoBestIndividualStat<Indi> bestSol;
  checkpoint.add(bestSol);
  monitor.add(generationCounter);
  monitor.add(timeStat);
  monitor.add(eval);
  monitor.add(bestStat);
  monitor.add(avgStdStat);
  monitor.add(bestSol);

  /* =========================================================
    *
    * L'algorithme.
    *
    * ========================================================= */
  eoEasyEA<Indi> solver(checkpoint, eval, breed, replace);

  /* =========================================================
    *
    * Lancement de l'algorithme.
    *
    * ========================================================= */
  for(unsigned i = 0; i < pop.size(); i++) {
    init(pop[i]);
    _eval(pop[i]);
  }
  solver(pop);
  pop.sort();
  return 0;
}