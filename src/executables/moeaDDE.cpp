#include <eo>
#include <moeo>
#include <es.h>
#include <DTLZ/src/PolynomialMutation.h>
#include <DTLZ/src/SBXCrossover.h>
#include "../utils/moeaDDE/customInit.h"
#include "../utils/moeaDDE/moeoMOEADDE.h"

// Fonction d'évaluation.
#include "../utils/evals/trafficEvalMO.h"

// Bornes solution.
#include "../utils/bounds.h"

// Type du vecteur objectif, stocke, entre autres, les fitness cibles à minimiser.
typedef moeoRealObjectiveVector<moeoObjectiveVectorTraits> ObjectiveVector;

// Type solution.
typedef moeoRealVector<ObjectiveVector> Indi;

/** Algorithme MOEA/D-DE. */
int main(int argc, char** argv) {
  /* =========================================================
    *
    * Paramètres.
    *
    * ========================================================= */
  eoParser parser(argc, argv);
  // Seed.
  uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();
  // Taille d'une solution.
  size_t d = 24;
  // Taille de population.
  uint32_t mu = parser.createParam(50, "popSize", "Population size", 'P', "Algorithm").value();
  // Facteur de mutation.
  double mutation_rate = parser.createParam(0.95, "pmut", "Mutation rate", 'M', "Algorithm").value();
  // Facteur de croisement.
  double xover_rate = parser.createParam(0.8, "xoverRate", "Crossover rate", 'X', "Algorithm").value();
  // Facteur de mutation par variable.
  double mutpervar_rate = parser.createParam(1.0, "vrate", "Mutation rate per variable", 'v', "Algorithm").value();
  // Paramètre eta pour la mutation polynomial (aussi appelée mutation GA).
  double mut_eta = parser.createParam(1.0, "meta", "eta parameter of polynomial mutation", 'm', "Algorithm").value();
  // Taille du voisinage de solution.
  unsigned int neighborhoodSize = parser.createParam(5, "neighborhoodSize", "Neighborhood size", 'N', "Algorithm").value();
  // Nombre de tentative remplacement par solution.
  unsigned int nRep = parser.createParam(4, "nRep", "N Rep", 'R', "Algorithm").value();
  // Paramètre Delta de l'algorithme.
  double delta = parser.createParam(0.6, "delta", "Delta", 'D', "Algorithm").value();
  // Critère d'arrêt : le temps.
  time_t duration = parser.createParam(60, "time", "Time limit stopping criterium (number of seconds)", 'T', "Algorithm").value();
  // Fichier permettant de stocker l'ensemble des paramètres d'algorithme.
  std::string str_status = parser.ProgramName() + ".status";
  eoValueParam<std::string> statusParam(str_status.c_str(), "status", "Status file");
  parser.processParam( statusParam, "Persistence" );

  make_verbose(parser);
  make_help(parser);

  /* =========================================================
    *
    * Fonction d'évaluation avec vecteur de fitness à minimiser.
    *
    * ========================================================= */
  // Deux fitness à minimiser.
  std::vector<bool> objVec(2, true);
  moeoObjectiveVectorTraits::setup(2, objVec);
  // Fonction d'évaluation.
  TrafficEvalMO<Indi> _eval;

  /* =========================================================
    *
    * Random seed.
    *
    * ========================================================= */
  rng.reseed(seed);

  /* =========================================================
    *
    * Initialisation.
    *
    * ========================================================= */
  // Initialisation de solutions avec bornes.
  eoRealInitBounded<Indi> init(Bounds::bounds);
  // Création de la population avec initialisation.
  eoPop<Indi> pop(mu, init);
  for(unsigned i = 0; i < pop.size(); i++) {
    init(pop[i]);
    _eval(pop[i]);
  }

  /* =========================================================
    *
    * Continuator: Critère d'arrêt.
    *
    * ========================================================= */
  eoTimeContinue<Indi> continuator(duration);

  /* =========================================================
    *
    * MOEA/D-DE : Ensemble de sous problèmes.
    *
    * ========================================================= */
  SubProblems<Indi> problems(pop, neighborhoodSize);
  //CustomInit<Indi> customInit(problems);
  //customInit(pop);

  /* =========================================================
    *
    * MOEA/D-DE : Définition du voisinage.
    *
    * ========================================================= */
  NeighborHood<Indi> neighborHood(pop, delta);

  /* =========================================================
    *
    * Opérateurs de variation.
    *
    * ========================================================= */
  // Opérateur de mutation DE (avec selection, mutation et réparation).
  DeRandMutation<Indi> mutation(pop, mutation_rate, Bounds::bounds);
  // Opérateur de croisement DE.
  DEXover<Indi> xover(pop, xover_rate);
  // Opérateur de mutation polynomial (GA mutation).
  PolynomialMutation<Indi> polynomialMutation(Bounds::bounds, mutpervar_rate, mut_eta);
  // Opérateur de variation modifié afin d'utiliser la mutation DE, le croisement DE et la mutation GA selon les sous-problèmes.
  CustomBreed<Indi> breed(mutation, xover, polynomialMutation, problems, neighborHood);

  /* =========================================================
    *
    * Remplacement.
    *
    * ========================================================= */
  CustomReplacement<Indi> replace(nRep, problems, neighborHood);

  /* =========================================================
    *
    * Sortie pour avoir l'avancement de l'algorithme.
    *
    * ========================================================= */
  eoCheckPoint<Indi> checkpoint(continuator);
  eoState outState;
  outState.registerObject(pop);
  eoCountedStateSaver stateSaver(1, outState, "../output/0/" + std::to_string(seed) + "/moeaDDE/generation_moeaDDE_", "dat");
  checkpoint.add(stateSaver);

  /* =========================================================
    *
    * L'algorithme.
    *
    * ========================================================= */
  moeoMOEADDE<Indi> solver(checkpoint, _eval, breed, replace);

  /* =========================================================
    *
    * Lancement de l'algorithme.
    *
    * ========================================================= */
  solver(pop);
  pop.sort();
  return 0;
}