#include <eo>
#include <edo>
#include <es.h>

// Fonction d'évaluation.
#include "../utils/evals/trafficEval.h"

// Bornes solution.
#include "../utils/bounds.h"

// Réparateur de solution.
#include "../utils/customRepairer.h"

// On cherche à minimiser une fitness.
typedef eoMinimizingFitness Fitness;

// Type solution.
typedef eoReal<Fitness> Indi;
using CMA = edoNormalAdaptive<Indi>;

/** Algorithme cmaES. */
int main(int argc, char** argv) {
  /* =========================================================
    *
    * Paramètres.
    *
    * ========================================================= */
  eoParser parser(argc, argv);
  // Seed.
  uint32_t seed = parser.getORcreateParam(time(0), "seed", "Random number seed", 'S').value();
  // Taille d'une solution.
  size_t d = 24;
  // Taille de population.
  uint32_t mu = parser.createParam(50, "popSize", "Population size", 'P', "Algorithm").value();
  // Critère d'arrêt : le temps.
  time_t duration = parser.createParam(60, "time", "Time limit stopping criterium (number of seconds)", 'T', "Algorithm").value();
  // Type d'évaluation (1 -> minimisation du temps | 2 -> minimisation du co2).
  int evalType = parser.createParam(1, "evalType", "Type d'évaluation : minimisation temps (1) ou co2 (2)", 'F', "Algorithm").value();
  // Fichier de sortie pour l'avancement de l'algorithme.
  std::string fileOutName = parser.createParam(std::string("../output/" + std::to_string(evalType) + "/" + std::to_string(seed) + "/" +  + "cmaES.csv"), "output", "Output file name to report statistics", 'o', "Ouput").value();
  // Fichier permettant de stocker l'ensemble des paramètres d'algorithme.
  std::string str_status = parser.ProgramName() + ".status";
  eoValueParam<std::string> statusParam(str_status.c_str(), "status", "Status file");
  parser.processParam( statusParam, "Persistence" );

  make_verbose(parser);
  make_help(parser);

  /* =========================================================
    *
    * Fonction d'évaluation.
    *
    * ========================================================= */
  TrafficEval<Indi> _eval(evalType);
  // Permet de compter le nombre d'évaluation.
  eoEvalFuncCounter<Indi> eval(_eval, "neval");
  // Evaluation de la population.
  eoPopLoopEval<Indi> pop_eval(eval);

  /* =========================================================
    *
    * Random seed.
    *
    * ========================================================= */
  rng.reseed(seed);

  /* =========================================================
    *
    * Initialisation.
    *
    * ========================================================= */
  eoRealInitBounded<Indi> init(Bounds::bounds);

  /* =========================================================
    *
    * Continuator: Critère d'arrêt.
    *
    * ========================================================= */
  eoTimeContinue<Indi> continuator(duration);
  edoContAdaptiveFinite<CMA> distrib_continue;

  /* =========================================================
    *
    * Selection de l'algorithme cmaES.
    *
    * ========================================================= */
  eoRankMuSelect<Indi> select(d / 2);

  /* =========================================================
    *
    * Opérateurs de variation.
    *
    * ========================================================= */
  edoNormalAdaptive<Indi> gaussian(d);
  edoEstimatorNormalAdaptive<Indi> estimator(gaussian);
  CustomRepairer<Indi> repairer(Bounds::bounds);
  edoSamplerNormalAdaptive<Indi> sampler(repairer);

  /* =========================================================
    *
    * Remplacement.
    *
    * ========================================================= */
  // (mu, lambda) - Strategie de remplacement.
  eoCommaReplacement<Indi> replace;
  // Initialisation de la population.
  eoPop<Indi> pop(mu, init);

  /* =========================================================
    *
    * Sortie pour avoir l'avancement de l'algorithme.
    *
    * ========================================================= */
  eoCheckPoint<Indi> checkpoint(continuator);
  eoFileMonitor monitor(fileOutName, " ", false, true);
  checkpoint.add(monitor);
  eoValueParam<unsigned> generationCounter(0, "iteration");
  eoIncrementor<unsigned> increment(generationCounter.value());
  checkpoint.add(increment);
  eoTimeCounter timeStat;
  checkpoint.add(timeStat);
  eoBestFitnessStat<Indi> bestStat("best");
  checkpoint.add(bestStat);
  eoSecondMomentStats<Indi> avgStdStat("avg std");
  checkpoint.add(avgStdStat);
  eoBestIndividualStat<Indi> bestSol;
  checkpoint.add(bestSol);
  monitor.add(generationCounter);
  monitor.add(timeStat);
  monitor.add(eval);
  monitor.add(bestStat);
  monitor.add(avgStdStat);
  monitor.add(bestSol);

  /* =========================================================
    *
    * L'algorithme.
    *
    * ========================================================= */
  edoAlgoAdaptive<CMA> solver(gaussian, pop_eval, select, estimator, sampler , replace, checkpoint, distrib_continue);

  /* =========================================================
    *
    * Lancement de l'algorithme.
    *
    * ========================================================= */
  for(unsigned i = 0; i < pop.size(); i++) {
    init(pop[i]);
    _eval(pop[i]);
  }
  solver(pop);
  pop.sort();
  return 0;
}