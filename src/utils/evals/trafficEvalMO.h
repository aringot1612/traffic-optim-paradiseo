#ifndef _TrafficEvalMO_h
#define _TrafficEvalMO_h

#include <core/moeoEvalFunc.h>
#include "../simulator.h"

/** Evaluation simulateur. */
template<class EOT> class TrafficEvalMO : public moeoEvalFunc<EOT>
{
  public:
    /**
     * Fonction d'évaluation
     *
     * @param _sol la solution à évaluer.
     */
    void operator() (EOT& solution) {
      typename EOT::ObjectiveVector objVec;
      std::vector<double> result = Simulator<EOT>::evaluate(solution);
      objVec[0] = result[1];
      objVec[1] = result[2];
      solution.objectiveVector(objVec);
    }
};
#endif