#ifndef _TrafficEval_h
#define _TrafficEval_h

#include <eoEvalFunc.h>
#include "../simulator.h"

/** Evaluation simulateur. */
template<class EOT> class TrafficEval : public eoEvalFunc<EOT>
{
  public:

    /** Constructeur */ 
    TrafficEval(int type) : type(type){}

    /**
     * Fonction d'évaluation
     *
     * @param _sol la solution à évaluer.
     */
    void operator() (EOT& solution) {
      // Récupèration des valeurs de fitness via simulateur et calcul d'une fitness globale à minimiser.
      solution.fitness(computeFitness(Simulator<EOT>::evaluate(solution)));
    }

    /**
     * Permet de calculer une fitness globale à partir d'un tableau de fitness.
     *
     * @param _sol le tableau de fitness à utiliser.
     * 
     * @return Une fitness globale à minimiser.
     */
    double computeFitness(std::vector<double> result){
      return result[type];
    }

  private:
    int type;
};
#endif