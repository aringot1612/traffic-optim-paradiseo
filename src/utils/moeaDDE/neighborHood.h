#ifndef _NeighborHood_h
#define _NeighborHood_h

#include <vector>

/** Classe permettant de définir le voisinage pour chaque individu d'une population. */
template<class MOEOT> class NeighborHood
{
  public:
    NeighborHood(const eoPop<MOEOT>& _pop, double _delta) : pop(_pop), delta(_delta) {
      specs = std::vector<bool>(0);
    }

    void resetParentsSpec(){
      specs.clear();
      for(unsigned int i = 0 ; i < pop.size() ; i++){
        specs.push_back(rng.uniform() <= delta);
      }
    }

    std::vector<bool> getParentsSpec(){
      return specs;
    }

  private:
    const eoPop<MOEOT>& pop;
    double delta;
    std::vector<bool> specs;
};
#endif