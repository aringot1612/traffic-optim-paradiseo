#ifndef _CustomReplacement_h
#define _CustomReplacement_h

#include <eoPop.h>
#include <eoReplacement.h>

/** Classe permettant de gérer les remplacements de solution dans la population. */
template <class MOEOT> class CustomReplacement : public eoReplacement<MOEOT>
{
  public:
    CustomReplacement(unsigned int _nRep, SubProblems<MOEOT>& _problems, NeighborHood<MOEOT>& _neighborHoodUtility) : nRep(_nRep), problems(_problems), neighborHoodUtility(_neighborHoodUtility) {
      init();
    }

    /* Remplacement des solutions en prennant les meilleures. */
    void operator()(eoPop<MOEOT>& _parents, eoPop<MOEOT>& _offspring)
    {
      init();
      std::vector<unsigned int> indexList(0);
      // On récupère le choix de voisinage pour chaque individu...
      std::vector<bool> parentsSpec = neighborHoodUtility.getParentsSpec();
      // Pour chaque individu...
      for(unsigned int i = 0 ; i < _offspring.size() ; i++){
        indexList.clear();
        // Récupèration du voisinage pour chaque individu...
        if(parentsSpec.at(i)){ // Voisinage restreint...
          indexList = problems.neighborDirections.at(i);
        }
        else{ // voisinage complet...
          for(unsigned int i = 0 ; i < _offspring.size() ; i++) {
            indexList.push_back(i);
          }
        }
        // c ← 1;
        c = 1;
        // Tant que le nombre d'essais remplacement n'est pas atteint, et qu'il reste des voisins... 
        while(c <= nRep && !indexList.empty()){
            // On choisit une valeur random bornée entre 0 et la taille du voisinage.
            rand = rng.random(indexList.size());
            // On récupère un individu du voisinage.
            j = indexList.at(rand);
            // On supprime l'index en question de la liste des voisins.
            indexList.erase(indexList.begin() + rand);
            // Si g(ui|wj , z∗) ≤ g(xj |wj , z∗) alors...
            // Vérification des scalar Eval selon la direction, entre la solution cible et la solution choisie aléatoirement parmi le voisinage.
            if(problems.directions.at(j)(_offspring.at(i)) <= problems.directions.at(j)(_parents.at(j))){
                // xj ← ui
                _parents[j] = _offspring.at(i);
                // c ← c + 1;
                c++;
            }
        };
      }
    }

  private:
    SubProblems<MOEOT> problems;
    NeighborHood<MOEOT>& neighborHoodUtility;
    std::vector<bool> parentsSpec;
    unsigned int nRep;
    unsigned int c;
    unsigned int rand;
    unsigned int j;

    void init(){
      c = 1;
      rand = 0;
      j = 0;
    }
};
#endif