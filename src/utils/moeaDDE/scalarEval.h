#ifndef __ScalarEval__
#define __ScalarEval__

/** Fonction d'évaluation scalaire. */
template<class MOEOT> class ScalarEval {
  public:
    ScalarEval(std::vector<double> & _direction, std::vector<double> & _refPoint) : direction(_direction), refPoint(_refPoint) {}

    /** Opérateur suivant la méthode : Weighted Tchebychef. */
    double operator()(MOEOT & _solution) {
      double max = 0.0;
      double tmp = 0.0;
      for(unsigned int i = 0 ; i < _solution.objectiveVector().size() ; i++){
        tmp = direction.at(i) * abs(refPoint.at(i) - _solution.objectiveVector().at(i));
        if(tmp > max)
          max = tmp;
      }
      return max;
    }
    std::vector<double> direction;
    std::vector<double> refPoint;
};

#endif