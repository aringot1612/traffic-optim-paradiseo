#ifndef __SubProblems__
#define __SubProblems__

#include <vector>
#include <math.h>
#include "./scalarEval.h"

/** Classe permettant d'initialiser les sous-problèmes. */
template<class MOEOT> class SubProblems {
  public:
    SubProblems(eoPop<MOEOT>& _pop, unsigned int _neighborhoodSize) : pop(_pop), neighborhoodSize(_neighborhoodSize){
      std::vector<double> refPoint = {0, 0};
      std::vector<double> direction(0);
      std::vector<unsigned> neighborhoodIndex(0);
      solutionsIndex = std::vector<unsigned>(0);
      bool even = (neighborhoodSize % 2 == 0);
      int globalOffset = (!even) ? neighborhoodSize / 2 : (neighborhoodSize - 1) / 2;
      int evenOffset = (even) ? 1 : 0;
      double theta = 0.0;
      // Pour chaque individu de la population...
      for(int i = 0 ; i < _pop.size() ; i++){
        int localIndex = 0;
        int localOffet = 0;
        // Initialisation du voisinage restreint.
        neighborhoodIndex.clear();
        // Attention, ce code permet de trouver les index pour l'ensemble du voisinage de façon à ce que chaque individu soit, au mieux, au centre de son voisinage.
        if((i - globalOffset - evenOffset) < 0)
          localOffet = globalOffset + (-1 * (i - globalOffset - evenOffset));
        else if (i + globalOffset + evenOffset + localOffet > _pop.size() - 1)
          localOffet = (_pop.size() - 1) - (i + globalOffset  + evenOffset + localOffet);
        if(localOffet > 0){
          int reverse = (localOffet + 1) - neighborhoodSize;
          for(int j = i ; j < neighborhoodSize + i ; j++){
            neighborhoodIndex.push_back(j + reverse);
          }
          localIndex = neighborhoodSize - localOffet - 1;
        }
        else if(localOffet == 0){
          for(int j =i - globalOffset ; j < i + globalOffset + localOffet + evenOffset + 1; j++){
            neighborhoodIndex.push_back(j);
          }
          localIndex = globalOffset;
        }
        else{
          for(int j = i - globalOffset + localOffet ; j < _pop.size(); j++){
            neighborhoodIndex.push_back(j);
          }
          localIndex = globalOffset + (localOffet * -1);
        }
        solutionsIndex.push_back(localIndex);
        neighborDirections.push_back(neighborhoodIndex);

        // Initialisation des directions.
        theta =  (double) i / (_pop.size() - 1.0) * M_PI / 2.0;
        direction.clear();
        direction = {cos(theta), sin(theta)};
        directions.push_back(ScalarEval<MOEOT>(direction, refPoint));
      }
    }
    std::vector<ScalarEval<MOEOT>> directions;
    std::vector<std::vector<unsigned>> neighborDirections;
    std::vector<unsigned> solutionsIndex;
  private:
    eoPop<MOEOT>& pop;
    unsigned int neighborhoodSize;
};

#endif