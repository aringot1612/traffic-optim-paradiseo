#ifndef MOEOMOEADDE_H_
#define MOEOMOEADDE_H_

#include <algo/moeoEA.h>
#include "./subProblems.h"
#include <iostream>
#include "../utils/deComponents/deRandMutation.h"
#include "../utils/deComponents/deXover.h"
#include "./NeighborHood.h"
#include "./CustomBreed.h"
#include "./CustomReplacement.h"

/** Algorithme MOEA/D-DE. */
template<class MOEOT> class moeoMOEADDE: public moeoEA<MOEOT>
{
  public:
    moeoMOEADDE (eoContinue<MOEOT>& _continuator, moeoEvalFunc<MOEOT>& _eval, CustomBreed<MOEOT>& _breed, CustomReplacement<MOEOT>& _replace) : continuator(_continuator), eval(_eval), loopEval(_eval), popEval(loopEval), breed(_breed), replace(_replace) {}

    /** Application de l'algorithme. */
    virtual void operator () (eoPop<MOEOT>& _pop)
    {
        // Création d'un ensemble de solution contenant des enfants, et une solution vide.
        eoPop<MOEOT> offspring, empty_pop;
        // Evaluation initiale.
        popEval(empty_pop, _pop);

        // Tant que le critère d'arrêt n'est pas atteint...
        do
        {
          // On vide la population d'enfants pour la génération.
          offspring.clear();
          // Application des variations afin d'obtenir un ensemble d'enfants.
          breed(_pop, offspring);
          // Application d'une évaluation sur la population et les enfants.
          popEval(_pop, offspring);
          // Remplacement d'individus dans la population.
          replace(_pop, offspring);
        }
        while (continuator (_pop));
    }

  private:
    /** Critère d'arrêt */
    eoContinue<MOEOT>& continuator;
    /** Fonction d'évaluation. */
    eoEvalFunc<MOEOT>& eval;
    /** Fonction d'évaluation pour toute la population. */
    eoPopLoopEval<MOEOT> loopEval;
    eoPopEvalFunc<MOEOT>& popEval;
    /** Opérateur de variation. */
    CustomBreed<MOEOT>& breed;
    /** Stratégie de remplacement. */
    CustomReplacement<MOEOT>& replace;
};

#endif