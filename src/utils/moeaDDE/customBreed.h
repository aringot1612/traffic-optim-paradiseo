#ifndef _CustomBreed_h
#define _CustomBreed_h

#include "eoBreed.h"
#include "../operators/deOp.h"

/** Classe permettant de gérer les variations appliquées sur une population (mutation, croisement et mutation GA). */
template <class MOEOT> class CustomBreed : public eoBreed<MOEOT>
{
  public:
    CustomBreed(DeRandMutation<MOEOT> & _mutation, DEXover<MOEOT> & _xover, PolynomialMutation<MOEOT>& _GAmutation, SubProblems<MOEOT>& _problems, NeighborHood<MOEOT>& _neighborHoodUtility) : mutation(_mutation), xover(_xover), GAMutation(_GAmutation), problems(_problems), neighborHoodUtility(_neighborHoodUtility){}

    void operator()(const eoPop<MOEOT>& _parents, eoPop<MOEOT>& _offspring)
    {
      // Autant d'enfants que de parents.
      _offspring.resize(_parents.size());
      // Index de la solution cible parmi le voisinage.
      unsigned int index = 0;
      // Liste d'index de voisinage.
      std::vector<unsigned> currentList(0);
      // On définit pour chaque individu, quelle sera son voisinage...
      neighborHoodUtility.resetParentsSpec();
      // On récupère le choix de voisinage.
      std::vector<bool> parentsSpec = neighborHoodUtility.getParentsSpec();
      // Pour chaque individu de la population...
      for(unsigned i = 0; i < _offspring.size(); i++) {
        // Création d'un voisinage.
        eoPop<MOEOT> neighborHood;
        // Si l'individu cible doit avoir comme voisinage, un ensemble de solution restreint...
        if(parentsSpec.at(i)){
          // On récupère les index des voisins correspondants.
          currentList = problems.neighborDirections.at(i);
          // On met à jour la taille du voisinage.
          neighborHood.resize(currentList.size());
          // Attribution des places de voisinage.
          for(unsigned int j = 0 ; j < currentList.size() ; j++){
            neighborHood[j] = _parents.at(currentList.at(j));
          }
          // On récupère l'index de la solution ciblée selon son voisinage.
          index = problems.solutionsIndex.at(i);
        }
        // Sinon, le voisinage de la solution ciblée est l'ensemble de la population...
        else{
          // Le voisinage -> tous les parents.
          neighborHood = _parents;
          // L'index de solution cible correspond à l'index de la solution dans les parents.
          index = i;
        }
        // On renseigne l'index à l'opérateur de mutation.
        mutation.index(index);
        // On renseigne l'index à l'opérateur de croisement.
        xover.index(index);
        // Application de la mutation DE.
        mutation(_offspring[i], neighborHood);
        // Application du croisement DE.
        xover(_offspring[i], neighborHood);
        // Application de la mutation GA.
        GAMutation(_offspring[i]);
      }
    }

  private:
    DeRandMutation<MOEOT>& mutation;
    DEXover<MOEOT>& xover;
    PolynomialMutation<MOEOT>& GAMutation;
    SubProblems<MOEOT> problems;
    NeighborHood<MOEOT>& neighborHoodUtility;
};
#endif