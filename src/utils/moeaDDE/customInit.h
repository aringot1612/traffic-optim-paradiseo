#ifndef _CustomInit_h
#define _CustomInit_h
#include "./subProblems.h"

/** Classe permettant de gérer l'initialisation de la population. */
template <class MOEOT> class CustomInit
{
  public:
    CustomInit(SubProblems<MOEOT>& _problems) :problems(_problems) {
    }

    /* Opérateur d'initialisation de population. */
    void operator()(eoPop<MOEOT>& _pop)
    {
      eoPop<MOEOT> basePop = _pop;
      for(unsigned int i = 0 ; i < problems.directions.size() ; i++){
        unsigned int index = 0;
        unsigned int bestIndex = -1;
        double bestScore = 0;
        for(unsigned int j = 0 ; j < basePop.size() ; j++){
          if(problems.directions.at(i)(basePop.at(j)) > bestScore){
            bestIndex = j;
            bestScore = problems.directions.at(i)(basePop.at(j));
          }
        }
        _pop[i] = basePop.at(bestIndex);
        basePop.erase(basePop.begin() + bestIndex);
      }
    }
  private:
    SubProblems<MOEOT> problems;
};
#endif