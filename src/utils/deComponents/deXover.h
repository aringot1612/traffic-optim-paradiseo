#ifndef _DEXover_h
#define _DEXover_h

#include "../operators/deOp.h"

/** Classe permettant de gérer les croisements de solution dans la population. */
template <class EOT> class DEXover : public DEOp<EOT>
{
  using DEOp<EOT>::parents;
  using DEOp<EOT>::id;

  public:
    DEXover(eoPop<EOT> & _parents, double _CR) : DEOp<EOT>(_parents), CR(_CR){}

    /* Croisement de solutions. */
    virtual bool operator()(EOT & _solution) {
      return computeXover(_solution, parents);
    }
    /* Croisement de solutions avec parents spécifiques. */
    bool operator()(EOT & _solution, eoPop<EOT> customParents) {
      return computeXover(_solution, customParents);
    }

  protected:
    // crossover rate
    double CR;

  private:
      bool computeXover(EOT& _solution, eoPop<EOT> _parents){
      unsigned jrand = rng.random(_solution.size());
      for(unsigned j = 0; j < jrand; j++) {
        if (rng.uniform() > CR)
          _solution[j] = _parents[id][j];
      }
      for(unsigned j = 0; j < jrand + 1; j++) {
        if (rng.uniform() > CR)
          _solution[j] = _parents[id][j];
      }
      _solution.invalidate();
      return true;
    }
};
#endif