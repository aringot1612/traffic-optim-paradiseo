#ifndef _DeRandMutation_h
#define _DeRandMutation_h

#include <vector>
#include <utility>
#include "../operators/deOp.h"

/** Classe permettant de gérer la mutation. */
template <class EOT> class DeRandMutation : public DEOp<EOT>
{
  using DEOp<EOT>::parents;
  using DEOp<EOT>::id;

  public:
    DeRandMutation(eoPop<EOT> & _parents, double _F, eoRealVectorBounds bounds) : DEOp<EOT>(_parents), F(_F), bounds(bounds){}

    /* Mutation de la solution.	*/
    virtual bool operator()(EOT & _solution) {
      return computeMutation(_solution, parents);
    }
    /* Mutation de la solution avec parents spécifiques.	*/
    bool operator()(EOT & _solution, eoPop<EOT> customParents) {
      return computeMutation(_solution, customParents);
    }

  protected:
    double F;
    private:
    eoRealVectorBounds bounds;

  private:
    bool computeMutation(EOT& _solution, eoPop<EOT> _parents){
      if (_parents.size() > 4) {
        unsigned k, i1, i2, i3;
        // random different indices
        std::vector<unsigned> indexes(_parents.size());
        for(unsigned i = 0; i < indexes.size(); i++){
          indexes[i] = i;
        }

        unsigned lastId = indexes.size() - 1;
        std::swap(indexes[id], indexes[lastId]);  // remove id
        lastId--;

        k = rng.random(lastId + 1);
        i1 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);
        lastId--;

        k = rng.random(lastId + 1);
        i2 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);
        lastId--;

        k = rng.random(lastId + 1);
        i3 = indexes[k];
        std::swap(indexes[k], indexes[lastId]);
        lastId--;

        // Gestion des bornes sur mutations.
        _solution.resize(_parents[id].size());
        for(unsigned j = 0; j < _solution.size(); j++){
          _solution[j] = _parents[i1][j] + F * (_parents[i2][j] - _parents[i3][j]);
          if(_solution[j] < bounds[j]->minimum())
            _solution[j] = bounds[j]->minimum();
          else if(_solution[j] > bounds[j]->maximum())
            _solution[j] = bounds[j]->maximum();
        }
        _solution.invalidate();
        return true;
      }
      return false;
    }
};
#endif