#ifndef _vectorUtility_h
#define _vectorUtility_h

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <algorithm>
#include <set>
#include <climits>

namespace fs = std::filesystem;

/** Classe contenant des méthodes utiles au traitement de vecteurs.
 * Utilisé massivement par : singleObjectiveProcessing, multiObjectiveProcessing et paretoFrontProcessing.
 */
class VectorUtility
{
	public:
    static int computeAvg(std::vector<int> values){
      int sum = 0;
      for(unsigned int i = 0 ; i < values.size() ; i++){
        sum += values.at(i);
      }
      return sum / values.size();
    }

    static std::vector<std::string> split(std::string text, char delim) {
      std::string line;
      std::vector<std::string> vec;
      std::stringstream ss(text);
      while(std::getline(ss, line, delim)) {
        vec.push_back(line);
      }
      return vec;
    }

    static std::string concatenate(std::vector<std::string> values){
      std::string s;
      for(unsigned int i = 0 ; i < values.size() ; i++){
        s += values.at(i) += " ";
      }
      return s;
    }

    static std::string concatenateInt(std::vector<int> values){
      std::string s;
      for(unsigned int i = 0 ; i < values.size() ; i++){
        s += std::to_string(values.at(i)) += " ";
      }
      return s;
    }

    static std::string concatenateSpecial(std::vector<int> values){
      std::string s;
        s += std::to_string(computeAvg(values)) += ",";
      for(unsigned int i = 0 ; i < values.size() ; i++){
        s += std::to_string(values.at(i)) += " ";
      }
      return s;
    }

    static bool checkInVector(std::vector<std::string> vector, std::string string){
      for(unsigned int i = 0 ; i < vector.capacity() ; i++){
        if (vector.at(i).find(string) != std::string::npos)
          return true;
      }
      return false;
    }

    static bool myCmp(std::string s1, std::string s2)
    {
      if (s1.size() == s2.size()) {return s1 < s2;}
      else {return s1.size() < s2.size();}
    }
};

#endif