#ifndef _fileReader_h
#define _fileReader_h

#include <string>
#include <fstream>
#include <vector>
#include <utility>
#include <stdexcept>
#include <sstream>
#include <iostream>

/** Classe venant permettant de lire des fichiers. */
class FileReader{
  private:
    std::ofstream myFile;

    std::vector<std::string> split(std::string text, char delim) {
      std::string line;
      std::vector<std::string> vec;
      std::stringstream ss(text);
      while(std::getline(ss, line, delim)) {
          vec.push_back(line);
      }
      return vec;
    }
    std::string getOnlyFitness(std::vector<std::string> values){
      std::string s;
      for(unsigned int i = 0 ; i < 2 ; i++){
          s += values.at(i) += " ";
      }
      return s;
    }
  public:
    std::vector<std::vector<int>> readFile(std::string filename){
      std::vector<std::vector<int>>result;
      std::ifstream myFile(filename);
      if(!myFile.is_open()) throw std::runtime_error("Could not open file");
      std::string line, colname;
      int val;
      int rowIdx = 0;
      while(std::getline(myFile, line))
      {
          std::vector<std::string> lineDetails = split(line, ' ');
          if(lineDetails.size() > 1){
              std::stringstream ss(getOnlyFitness(lineDetails));
              result.push_back({});
              while(ss >> val){
                  result[rowIdx].push_back(val);
                  if(ss.peek() == ',') ss.ignore();
              }
              rowIdx++;
          }
      }
      myFile.close();
      return result;
    }
};
#endif