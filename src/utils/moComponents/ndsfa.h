#ifndef _ndsfa_h
#define _ndsfa_h

#include "solution.h"

/** Classe permettant de filtrer les solutions non dominées parmi une population. */
class Ndsfa{
  public:
    /**
     * @brief Permet de retourner les solutions non dominées parmi un ensemble de solution.
     * 
     * @param solutions L'ensemble de solution à utiliser pour le filtrage.
     * @return std::vector<Solution> L'ensemble de solutions non dominées.
     */
    std::vector<Solution> findNonDominatedSolutions(std::vector<Solution> solutions){
      // Vecteur de solution non dominées vide.
      std::vector<Solution> nonDominatedSolutions;
      // Pour chaque solution...
      for(unsigned int i = 0 ; i < solutions.size() ; i++){
        // On utilise un index secondaire à zero pour parcourir les solutions.
        int sIndex = 0;
        // On récupére la première solution : solution temporaire.
        Solution s = solutions[sIndex];
        // On considère la solution courante comme non dominée.
        bool dominated = false;
        // Tant que la solution courante est non dominée et qu'il reste des solutions à comparer...
        while(!dominated && sIndex < solutions.size()){
          // Si la solution courante est bien dominée par la solution temporaire...
          if(solutions[i].isDominatedBy(s))
            dominated = true;
          // Sinon, la solution courante domine la solution temporaire...
          else {
            // On passe à la solution temporaire suivante.
            sIndex++;
            if(sIndex < solutions.size())
              s = solutions[sIndex];
          }
        }
        // Si la solution courante reste dominée jusque là...
        if(!dominated)
          // On l'ajoute à la liste des solutions non dominées.
          nonDominatedSolutions.push_back(solutions[i]);
      }
      // On retourne la liste des solutions non dominées.
      return nonDominatedSolutions;
    }
};
#endif