from cProfile import label
import os
from fnmatch import fnmatch
import matplotlib.pyplot as plt3
import numpy as np
import scipy.stats as stats

timeTicks = np.arange(150000, 207000, 7000)
co2Ticks = np.arange(500000, 760000, 28000)
hvTicks = np.arange(5000000000, 16000000000, 1000000000) # 5739892145 - 14728322082

resultFileName = 'results/mo/pValues.txt'

def computeParetoFront(folderName):
    _files = []
    for path, subdirs, files in os.walk(folderName):
        for name in files:
            _files.append(os.path.join(path, name))
    for file in _files:
        title = file.split('/')[4].split('.')[0]
        titleDetails = title.split('_')
        algorithm = titleDetails[2]
        seed = titleDetails[0]
        gen = titleDetails[3]
        f = open(file)
        data = f.readlines()
        plt3.scatter(x=list(map(int, data[0].split(' ')[:-1])), y=list(map(int, data[1].split(' ')[:-1])), color='c', label='solution')
        plt3.scatter(x=list(map(int, data[2].split(' ')[:-1])), y=list(map(int, data[3].split(' ')[:-1])), color='r', label='paretto front')
        plt3.legend(loc='upper center')
        plt3.xticks(timeTicks)
        plt3.yticks(co2Ticks)
        plt3.xlabel('time')
        plt3.ylabel('co2')
        plt3.subplots_adjust(left=0.15, right=0.85)
        plt3.title('Population de ' + algorithm + ' au bout de ' + gen + ' generations (seed : ' + seed + ')', y=1.05)
        plt3.savefig('results/mo/best_' + algorithm + '.png', dpi=300)
        plt3.clf()

def exportPvalue(pvalue, details):
    fWrite = open(resultFileName, "a")
    fWrite.write("\nP-value entre l\'algorithme " + details + " : " + str(pvalue))
    fWrite.close()

def computePValues(data):
    _, pvalue1 = stats.mannwhitneyu(data[0], data[1])
    _, pvalue2 = stats.mannwhitneyu(data[0], data[2])
    _, pvalue3 = stats.mannwhitneyu(data[1], data[2])
    exportPvalue(pvalue1, "IBEA et MOEA/D-DE")
    exportPvalue(pvalue2, "IBEA et nsgaII")
    exportPvalue(pvalue3, "MOEA/D-DE et nsgaII")

def computeHV(folderName):
    _files = []
    totalAvgList = []
    for path, subdirs, files in os.walk(folderName):
        for name in files:
            _files.append(os.path.join(path, name))
    for file in _files:
        f = open(file)
        algorithm = file.split('/')[4].split('.')[0]
        data = f.readlines()
        f.close()
        subPlotXList = []
        subPlotYList = []
        avgList = []
        nbGen = 1
        maxSeed = 0
        for dataLine in data:
            maxSeed += 1
        for dataLine in data:
            tmpForAvgData = dataLine.replace('NA ', '')
            tmpForAvgData = tmpForAvgData.replace('NA', '')
            avgList.append(int(np.average(list(map(int, (tmpForAvgData.strip().split(' ')))))))
            needBox = nbGen % 10
            if needBox == 0 or nbGen == 1:
                if dataLine.count('NA') < (maxSeed / 2):
                    dataLine = dataLine.replace('NA ', '')
                    dataLine = dataLine.replace('NA', '')
                    subPlotXList.append(nbGen)
                    subPlotYList.append(list(map(int, (dataLine.strip().split(' ')))))
            nbGen += 1
        if nbGen > 1:
            plt3.boxplot(subPlotYList, positions=subPlotXList, widths=2)
            plt3.xlabel('Generation')
            plt3.ylabel('hypervolume')
            plt3.xticks(subPlotXList)
            plt3.yticks(hvTicks)
            plt3.subplots_adjust(left=0.15, right=0.85)
            plt3.title('Indicateur d\'hypervolume pour l\'algorithme ' + algorithm, y=1.05)
            plt3.savefig('results/mo/' + algorithm + '.png', dpi=300)
            plt3.clf()
        totalAvgList.append(avgList)
    computePValues(totalAvgList)

def main():
    if os.path.exists(resultFileName):
        os.remove(resultFileName)
    computeHV("results/data/mo/hv")
    computeParetoFront("results/data/mo/algorithms")
main()