/*
* <moeoNSGAII.h>
*
*/
//-----------------------------------------------------------------------------

#ifndef MOEOMOEADDE_H_
#define MOEOMOEADDE_H_

#include <algo/moeoEA.h>
#include "./subProblems.h"
#include <iostream>
#include "../utils/deComponents/deRandMutation.h"
#include "../utils/deComponents/deXover.h"
#include "../utils/bounds.h"

/**
 * NSGA-II (Non-dominated Sorting Genetic Algorithm II).
 * Deb, K., S. Agrawal, A. Pratap, and T. Meyarivan. A fast elitist non-dominated sorting genetic algorithm for multi-objective optimization: NSGA-II. IEEE Transactions on Evolutionary Computation, Vol. 6, No 2, pp 182-197 (2002).
 * This class builds the NSGA-II algorithm only by using the fine-grained components of the ParadisEO-MOEO framework.
 */
template<class MOEOT> class moeoMOEADDE: public moeoEA<MOEOT>
{
public:
    moeoMOEADDE (eoContinue<MOEOT>& _continuator, moeoEvalFunc<MOEOT>& _moEval, eoRealVectorBounds _bounds) : continuator(_continuator), moEval(_moEval), bounds(_bounds){}

    /**
     * Apply a the algorithm to the population _pop until the stopping criteria is satified.
     * @param _pop the population
     */
    virtual void operator () (eoPop < MOEOT > &_pop)
    {
        // t ← 1, initialize the population P = {x1, ..., xµ}; (done)

        // Some parameters here.
        int neighborhoodSize = 5;
        unsigned int nRep = 2;
        double sigma = 0.8;
        double mutationFactor = 0.8;
        double xoverRate = 0.6;
        double mutpervar_rate = 0.9;
        double mut_eta = 0.9;

        // Initialization of the algorithm.
        SubProblems<MOEOT> problems;
        std::vector<double> refPoint = {0, 0};
        eoPop<MOEOT> parentsPop;
        std::vector<unsigned> neighborhoodIndex(0);
        std::vector<double> direction(0);
        unsigned int c = 1;
        unsigned int rand = 0;
        unsigned int j = 0;
        double val = 0;

        // for i ∈ {1, ..., µ} do...
        for(unsigned int i = 0 ; i < _pop.size() ; i++){
            // Set the neighborhood index list Bi = {i1, ..., iT };
            neighborhoodIndex.clear();
            if((i + neighborhoodSize - 1) < _pop.size()){
                for(unsigned int j = i ; j < (i + neighborhoodSize); j++){
                    neighborhoodIndex.push_back(j);
                }
            }
            else{
                for(unsigned int j = _pop.size() - neighborhoodSize ; j < _pop.size(); j++){
                    neighborhoodIndex.push_back(j);
                }
            }
            problems.neighborDirections.push_back(neighborhoodIndex);

            // Initialization directions.
            val = 1./_pop.size()*(i+1);
            direction.clear();
            direction = {1 - val, val};
            problems.directions.push_back(ScalarEval<MOEOT>(direction, refPoint, moEval));
        }

        // Preparation of R...
        std::vector<unsigned> currentIndexList(0); // This is R (empty).
        std::vector<unsigned> fullIndexList(0); // This is R when rand[0, 1] > δ
        unsigned int currentIndex = 0; // This is the current index xi according to parents.
        // filling fullIndexList...
        for(unsigned int i = 0 ; i < _pop.size() ; i++) {
            fullIndexList.push_back(i);
        }

        // while The termination criteria are not met do...
        do
        {
            // for i ∈ {1, ..., µ} do...
            for(unsigned int i = 0 ; i < _pop.size() ; i++){
                // if rand[0, 1] ≤ δ then...
                if(rng.uniform() <= sigma){
                    // R ← Bi;
                    currentIndexList = problems.neighborDirections.at(i);
                    currentIndex = 0; // The current index is 0 according to the parents. (The neighborhood of each solution contains the solution itself at index 0).
                }
                else{
                    // R ← {1, ..., µ};
                    currentIndexList = fullIndexList;
                    currentIndex = i;
                }
                // We need to provide a parents list from the currentIndexList.
                parentsPop.clear();
                for(unsigned int j = 0 ; j < currentIndexList.size() ; j++){
                    parentsPop.push_back(_pop.at(currentIndexList.at(j)));
                }
                // DE Mutation operator with : selection, mutation and repair from DE algorithm.
                DeRandMutation<MOEOT> mutation(parentsPop, mutationFactor, Bounds::bounds);
                // DE Crossover operator.
                DEXover<MOEOT> xover(parentsPop, xoverRate);
                // Polynomial mutation (GA mutation operator).
                PolynomialMutation<MOEOT> polynomialMutation(Bounds::bounds, mutpervar_rate, mut_eta);
                // Initial : xi.
                MOEOT u = parentsPop.at(currentIndex);
                // DE Mutation.
                mutation.index(currentIndex);
                mutation(u);
                // DE Crossover.
                xover.index(currentIndex);
                xover(u);
                // GA mutation.
                polynomialMutation(u);
                // c ← 1;
                c = 1;
                // while c ≤ nrep and R %= ∅ do...
                while(c <= nRep && !currentIndexList.empty()){
                    // Randomly select an index j from R
                    rand = rng.random(currentIndexList.size());
                    // R ← R\{j};
                    j = currentIndexList.at(rand);
                    currentIndexList.erase(currentIndexList.begin() + rand);
                    // if g(ui|wj , z∗) ≤ g(xj |wj , z∗) then...
                    if(problems.directions.at(j)(u) <= problems.directions.at(j)(_pop.at(j))){
                        // xj ← ui
                        _pop[j] = u;
                        // c ← c + 1;
                        c++;
                    }
                };
            }
        }
        while (continuator (_pop));
    }

protected:
    /** stopping criteria. */
    eoContinue < MOEOT > & continuator;
    /** evaluation function. */
    moeoEvalFunc < MOEOT > & moEval;
    /** Solution bounds for operators. */
    eoRealVectorBounds bounds;
};

#endif