#ifndef _CustomXover_h
#define _CustomXover_h

#include "../operators/deOp.h"

/** Classe permettant de gérer les croisements de solution. */
template <class MOEOT> class CustomXover : public DEOp<MOEOT>
{
	public:
		CustomXover(eoPop<MOEOT> & _parents, double _CR) : DEOp<MOEOT>(_parents), CR(_CR){}

		/* Croisement de solutions. */
		virtual bool operator()(MOEOT & parent, MOEOT & mutant, MOEOT & offspring) {
			unsigned jrand = rng.random(parent.size());
			for(unsigned j = 0; j < parent.size(); j++) {
				if (j == jrand || rng.uniform() < CR)
					offspring[j] = mutant[j];
        else
          offspring[j] = parent[j];
			}
			offspring.invalidate();
			return true;
		}

	protected:
		double CR;
};
#endif