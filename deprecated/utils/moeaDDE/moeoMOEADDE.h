/*
* <moeoNSGAII.h>
*
*/
//-----------------------------------------------------------------------------

#ifndef MOEOMOEADDE_H_
#define MOEOMOEADDE_H_

#include <eoBreed.h>
#include <eoCloneOps.h>
#include <eoContinue.h>
#include <eoEvalFunc.h>
#include <eoGenContinue.h>
#include <eoGeneralBreeder.h>
#include <eoGenOp.h>
#include <eoPopEvalFunc.h>
#include <eoSGAGenOp.h>
#include <algo/moeoEA.h>
#include <diversity/moeoFrontByFrontCrowdingDiversityAssignment.h>
#include <fitness/moeoDominanceDepthFitnessAssignment.h>
#include <replacement/moeoElitistReplacement.h>
#include <selection/moeoDetTournamentSelect.h>
#include "./subProblems.h"
#include "./customXOver.h"
#include <iostream>
#include "../utils/deComponents/deRandMutation.h"
#include "../utils/deComponents/deXover.h"
#include "../utils/deComponents/deBreed.h"
#include "../utils/bounds.h"

/**
 * NSGA-II (Non-dominated Sorting Genetic Algorithm II).
 * Deb, K., S. Agrawal, A. Pratap, and T. Meyarivan. A fast elitist non-dominated sorting genetic algorithm for multi-objective optimization: NSGA-II. IEEE Transactions on Evolutionary Computation, Vol. 6, No 2, pp 182-197 (2002).
 * This class builds the NSGA-II algorithm only by using the fine-grained components of the ParadisEO-MOEO framework.
 */
template < class MOEOT >
class moeoMOEADDE: public moeoEA < MOEOT >
{
public:
    moeoMOEADDE (eoContinue < MOEOT > & _continuator, eoEvalFunc < MOEOT > & _eval, moeoEvalFunc<MOEOT> & _moEval, eoTransform < MOEOT > & _transform, eoRealVectorBounds _bounds) :
            defaultGenContinuator(0), continuator(_continuator), eval(_eval), moEval(_moEval), defaultPopEval(_eval), popEval(defaultPopEval),
            select(2),  selectMany(select, 1.0), selectTransform(selectMany, _transform), bounds(_bounds), defaultSGAGenOp(defaultQuadOp, 0.0, defaultMonOp, 0.0), genBreed(select, defaultSGAGenOp)/*, breed(selectTransform)*/, replace(fitnessAssignment, diversityAssignment)
    {}
    /**
     * Apply a the algorithm to the population _pop until the stopping criteria is satified.
     * @param _pop the population
     */
    virtual void operator () (eoPop < MOEOT > &_pop)
    {

        /** parameters here. */
        int neighborhoodSize = 5;
        double sigma = 0.5;
        double mutationFactor = 0.8;
        double crossOverRate = 0.8;
        double xoverRate = 0.8;

        // Init -> done.
        eoPop<MOEOT> offspring, empty_pop;
        eoPop<MOEOT> parentsPop;
        popEval(empty_pop, _pop);	// a first eval of _pop

        // Set neighborhood index list.
        std::vector<std::vector<unsigned int>> neighborhoodIndexList(0);
        for(unsigned int i = 0 ; i < _pop.size() ; i++){
            std::vector<unsigned int> neighborhoodIndex(0);
            if(i + neighborhoodSize < _pop.size() - 1){
                for(unsigned int j = i ; j < i + neighborhoodSize; j++){
                    neighborhoodIndex.push_back(j);
                }
            }
            neighborhoodIndexList.push_back(neighborhoodIndex);
        }

        std::vector<unsigned int> currentIndexList(0);
        std::vector<unsigned int> fullIndexList(0);
        for(unsigned int i = 0 ; i < _pop.size() ; i++) {
            fullIndexList.push_back(i);
        }
        // Main loop.
        do
        {
            for(unsigned int i = 0 ; i < _pop.size() ; i++){
                // Creation of the parentsList from the neighborhoodIndexList.
                if(rng.uniform() <= sigma)
                    currentIndexList = neighborhoodIndexList.at(i);
                else
                    currentIndexList = fullIndexList;

                // Mutation from DE (we are using the parents list and not the entire population).
                parentsPop.clear();
                for(unsigned int j = 0 ; j < currentIndexList.size() ; j++){
                    parentsPop.push_back(_pop.at(j));
                }
                /*// Mutation operator with : selection, mutation and repair from DE algorithm.
                DeRandMutation<MOEOT> mutationOperator(parentsPop, mutationFactor, Bounds::bounds);
                // Custom XOver operator for this algorithm.
                CustomXover<MOEOT> xover(parentsPop, crossOverRate);
                MOEOT x = _pop.at(i);
                MOEOT v = _pop.at(i);
                MOEOT u = _pop.at(i);
                mutationOperator(v);
                xover(x, v, u);
                std::cout << "\n\n" ;
                std::cout << "x:   " << x << std::endl;
                std::cout << "v:   " << v << std::endl;
                std::cout << "u:   " << u << std::endl;*//*
                DeRandMutation<MOEOT> mutationOperator(parentsPop, mutationFactor, Bounds::bounds);
                DEXover<MOEOT> xoverOperator(parentsPop, xoverRate);
                DEBreed<MOEOT> breed(mutationOperator, xoverOperator);
                MOEOT x = _pop.at(i);*/
                // DE Mutation
                DeRandMutation<MOEOT> mutation(parentsPop, mutationFactor, Bounds::bounds);
                // DE crossover 
                DEXover<MOEOT> xover(parentsPop, xoverRate);
                // DE Breed
                //DEBreed<MOEOT> breed(mutation, xover);
                // New 
                MOEOT v = _pop.at(i);
                std::cout << "x:   " << v << std::endl;
                mutation.index(i);
				mutation(v);
				xover.index(i);
				xover(v);
                std::cout << "v:   " << v << std::endl;
            }
        }
        while (continuator (_pop));

        SubProblems<MOEOT> problems;
        std::vector<double> refPoint = {220000, 800000};
        for(unsigned int i = 0 ; i < _pop.size() ; i++){
            double val = 1./_pop.size()*(i+1);
            std::vector<double> direction = {1 - val, val};
            ScalarEval<MOEOT> scalarEval(direction, refPoint, moEval);
            std::vector<unsigned> nDirections = {-1, 0, 1};
            problems.directions.push_back(scalarEval);
            problems.neighborDirections.push_back(nDirections);
        }
    }

protected:
    /** a continuator based on the number of generations (used as default) */
    eoGenContinue < MOEOT > defaultGenContinuator;
    /** stopping criteria */
    eoContinue < MOEOT > & continuator;
    /** default eval */
    class DummyEval : public eoEvalFunc < MOEOT >
    {
    public:
        void operator()(MOEOT &) {}
    }
    defaultEval;
    /** evaluation function */
    eoEvalFunc < MOEOT > & eval;
    moeoEvalFunc < MOEOT > & moEval;
    /** default popEval */
    eoPopLoopEval < MOEOT > defaultPopEval;
    /** evaluation function used to evaluate the whole population */
    eoPopEvalFunc < MOEOT > & popEval;
    /** default select */
    class DummySelect : public eoSelect < MOEOT >
    {
    public :
        void operator()(const eoPop<MOEOT>&, eoPop<MOEOT>&) {}
    }
    defaultSelect;
    /** binary tournament selection */
    moeoDetTournamentSelect < MOEOT > select;
    /** default select many */
    eoSelectMany < MOEOT >  selectMany;
    /** select transform */
    eoSelectTransform < MOEOT > selectTransform;
    /** a default crossover */
    eoQuadCloneOp < MOEOT > defaultQuadOp;
    /** a default mutation */
    eoMonCloneOp < MOEOT > defaultMonOp;
    /** an object for genetic operators (used as default) */
    eoSGAGenOp < MOEOT > defaultSGAGenOp;
    /** default transform */
    class DummyTransform : public eoTransform < MOEOT >
    {
    public :
        void operator()(eoPop<MOEOT>&) {}
    }
    defaultTransform;
    /** general breeder */
    eoGeneralBreeder < MOEOT > genBreed;
    /** breeder */
    //eoBreed < MOEOT > & breed;
    /** fitness assignment used in NSGA */
    moeoDominanceDepthFitnessAssignment < MOEOT > fitnessAssignment;
    /** diversity assignment used in NSGA-II */
    moeoFrontByFrontCrowdingDiversityAssignment  < MOEOT > diversityAssignment;
    /** elitist replacement */
    moeoElitistReplacement < MOEOT > replace;
    eoRealVectorBounds bounds;
};

#endif