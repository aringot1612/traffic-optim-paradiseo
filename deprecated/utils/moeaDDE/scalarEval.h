#ifndef __ScalarEval__
#define __ScalarEval__

template<class MOEOT>
class ScalarEval : eoEvalFunc<MOEOT> {
public:
	ScalarEval(std::vector<double> & _direction, std::vector<double> & _refPoint, moeoEvalFunc<MOEOT> & _moEval) : direction(_direction), refPoint(_refPoint), moEval(_moEval) {}

	virtual void operator()(MOEOT & _solution) {
		moEval(_solution);
		double max = 0.0;
		double tmp = 0.0;
		for(unsigned int i = 0 ; i < _solution.objectiveVector().size() ; i++){
			tmp = direction.at(i) * abs(refPoint.at(i) - _solution.objectiveVector().at(i));
			if(tmp > max)
				max = tmp;
		}
		_solution.fitness(max);
	}

//protected:
	std::vector<double> direction;
	std::vector<double> refPoint;
	moeoEvalFunc<MOEOT> & moEval;
};

#endif