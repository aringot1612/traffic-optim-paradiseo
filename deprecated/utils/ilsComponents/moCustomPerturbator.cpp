#ifndef _moCustomPerturb_cpp
#define _moCustomPerturb_cpp

#include <eoEvalFunc.h>
#include <eoInit.h>
#include <perturb/moPerturbation.h>
#include <memory/moCountMoveMemory.h>

/** Classe permettant de personnaliser le perturbateur d'un algo ILS. (Correspond au perturbateur de base pour le moment). */
template <class Neighbor> class moCustomPerturbator : public moPerturbation<Neighbor>, public moCountMoveMemory<Neighbor> {
	public:
		typedef typename Neighbor::EOT EOT;
		/**
		 * Constructor
		 * @param _initializer an initializer of solution
		 * @param _fullEval a full evaluation function
		 * @param _threshold maximum number of iteration with no improvement
		 */
		moCustomPerturbator(eoInit<EOT>& _initializer, eoEvalFunc<EOT>& _fullEval, unsigned int _threshold):initializer(_initializer), fullEval(_fullEval), threshold(_threshold) {}

		/**
		 * Apply restart when necessary
		 * @param _solution to restart
		 * @return true
		 */
		bool operator()(EOT& _solution) {
			if ((*this).getCounter()>= threshold) {
				initializer(_solution);
				fullEval(_solution);
				(*this).initCounter();
			}
			return true;
		}

	private:
		eoInit<EOT>& initializer;
		eoEvalFunc<EOT>& fullEval;
		unsigned int threshold;
};
#endif