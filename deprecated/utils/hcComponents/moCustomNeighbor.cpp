#ifndef moCustomNeighbor_cpp
#define moCustomNeighbor_cpp

#include <neighborhood/moBackableNeighbor.h>
#include <neighborhood/moIndexNeighbor.h>
#include <random>
#include <chrono>
#include "../bounds.cpp"

/** Classe permettant de gérer les voisins d'une solution : Comment sont-ils trouvés ? */
template <class Fitness> class Neighbor : public moBackableNeighbor<eoReal<Fitness>>, public moIndexNeighbor<eoReal<Fitness>>
{
	public:
		typedef eoReal<Fitness> EOT;
		using moBackableNeighbor<EOT>::fitness;
		using moIndexNeighbor<EOT>::key;
		std::mt19937 gen;
		std::bernoulli_distribution boolDist;

		Neighbor(){boolDist = std::bernoulli_distribution(0.5);}

		/** Est appelée n fois pour trouver un voisin. */
		virtual void move(EOT & _solution) {
			gen = std::mt19937(std::chrono::steady_clock::now().time_since_epoch().count());
			double oldVal = _solution[key];
			double newVal = _solution[key];
			// On tente de changer une seule valeur, en s'assurant que la nouvelle valeur respecte les bornes.
			do{
				// Positif ou negatif
				if(boolDist(gen)){
					newVal += (Bounds::bounds[key]->maximum() - Bounds::bounds[key]->minimum())/12;
				}
				else{
					newVal -= (Bounds::bounds[key]->maximum() - Bounds::bounds[key]->minimum())/12;
				}
			// On continue de chercher une valeur si l'une des trois conditions n'est pas respectée.
			}while(newVal < Bounds::bounds[key]->minimum() || newVal > Bounds::bounds[key]->maximum() || newVal == oldVal);
			_solution[key] = newVal;
			_solution.invalidate();
		}

		virtual void moveBack(EOT & _solution) {
			move(_solution);
		}

		virtual std::string className() const {
			return "MoCustomNeighbor";
		}

		/**
		 * Read object.\
		 * Calls base class, just in case that one had something to do.
		 * The read and print methods should be compatible and have the same format.
		 * In principle, format is "plain": they just print a number
		 * @param _is a std::istream.
		 * @throw runtime_std::exception If a valid object can't be read.
		 */
		virtual void readFrom(std::istream& _is) {
			std::string fitness_str;
			int pos = _is.tellg();
			_is >> fitness_str;
			if (fitness_str == "INVALID") {
				throw eoInvalidFitnessError("invalid fitness");
			} else {
				Fitness repFit;
				_is.seekg(pos);
				_is >> repFit;
				_is >> key;
				fitness(repFit);
			}
		}

		/**
		 * Write object. Called printOn since it prints the object _on_ a stream.
		 * @param _os A std::ostream.
		 */
		virtual void printOn(std::ostream& _os) const {
			_os << fitness() << ' ' << key << std::endl;
		}
};
#endif