//-----------------------------------------------------------------------------
// Programme principal.
//-----------------------------------------------------------------------------

#define HAVE_SSTREAM

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>

// Paradiseo : composante principale.
#include <eo>
#include <es.h>

using namespace std;

//-----------------------------------------------------------------------------
// Fonction d'évaluation de voisinage.
#include <eval/moFullEvalByModif.h>

//-----------------------------------------------------------------------------
// Types d'exploration voisinage.
#include <neighborhood/moRndWithReplNeighborhood.h> // visit all neighbors in random order with neighbor
#include <neighborhood/moOrderNeighborhood.h> // visit all neighbors in order with neighbor
#include <neighborhood/moDummyNeighbor.h>

//-----------------------------------------------------------------------------
// Critères d'arrêt.
#include <continuator/moBestNoImproveContinuator.h>

//-----------------------------------------------------------------------------
// Algorithmes.
#include <algo/moSimpleHC.h>
#include <algo/moILS.h>

#include "../utils/ilsComponents/moCustomPerturbator.cpp" // Types de perturbateurs de solution.
#include "../utils/hcComponents/moCustomNeighbor.cpp"
#include "../utils/evals/trafficEval.h" // Type d'évaluation complète.

// Déclaration de types.
//-----------------------------------------------------------------------------
typedef eoMaximizingFitness Fitness;
typedef eoReal<Fitness> Indi;

// Programme principale -> Détails.
//-----------------------------------------------------------------------------
void main_function(int argc, char **argv)
{
    // Parser
    eoParser parser(argc, argv);
    eoValueParam<uint32_t> seedParam(time(0), "seed", "Random number seed", 'S');
    parser.processParam( seedParam );
    unsigned seed = seedParam.value();
    if (parser.userNeedsHelp()) {
        parser.printHelp(cout);
        exit(1);
    }
    // Seed.
    rng.reseed(seed);

    // Taille de solution.
    unsigned vecSize = 24;

    // Initialisation de la solution.
    Indi solution;
    std::vector<double> lowerBounds = {1.2, 0.1, 1.2, 0.1, 0.5, 1.2, 0.5, 0.7, 0.1, 0.2, 0.7, 0.7, 20, 10, 20, 25, 10, 25, 25, 25, 20, 10, 30, 30};
    std::vector<double> upperBounds = {1.8, 0.3, 1.8, 0.3, 1, 1.8, 1, 1.3, 0.3, 0.4, 1.3, 1.3, 30, 20, 30, 35, 20, 35, 35, 35, 30, 20, 35, 35};
    eoRealVectorBounds bounds(lowerBounds, upperBounds);
    eoRealInitBounded<Indi> init(bounds);

    // Evaluation de la solution.
    TrafficEval<Indi> fullEval;
    fullEval(solution);

    // Evaluation des voisins de solution.
    moFullEvalByModif<Neighbor> neighborEval(fullEval);

    // Déclaration du voisinage de la solution.
    moOrderNeighborhood<Neighbor> neighborhood(vecSize);

    // Perturbateur de la solution (pour ILS).
    moCustomPerturbator<Neighbor> perturbator(init, fullEval, 0);

    // Critères d'arrêts.
    moBestNoImproveContinuator<Neighbor> continuator(solution, 10);
    moBestNoImproveContinuator<moDummyNeighbor<Indi>> cont(solution, 10);

    // Algorithmes (choix de l'algorithme à utiliser via commentaires).

    moSimpleHC<Neighbor> algo(neighborhood, fullEval, neighborEval, continuator);
    moILS<Neighbor> ils(algo, fullEval, perturbator, cont);

    // Sortie -> Solution initiale.
    std::cout << "\n\nInitial: " << solution << std::endl ;

    // Lancement de l'algorithme.
    ils(solution);

    // Sortie -> Solution finale.
    std::cout << "Final:   " << solution << std::endl ;
}

int main(int argc, char **argv)
{
    try {
        main_function(argc, argv);
    }
    catch (exception& e) {
        cout << "Exception: " << e.what() << '\n';
    }
    return 1;
}