#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <algorithm>
#include <set>
#include <eo>
#include "omp.h"
#include "../utils/indicators.h"
namespace fs = std::filesystem;

std::vector<std::string> split(std::string text, char delim) {
    std::string line;
    std::vector<std::string> vec;
    std::stringstream ss(text);
    while(std::getline(ss, line, delim)) {
        vec.push_back(line);
    }
    return vec;
}

std::string concatenate(std::vector<std::string> values){
	std::string s;
	for(unsigned int i = 0 ; i < values.size() ; i++){
		s += values.at(i) += " ";
	}
	return s;
}

std::string concatenateInt(std::vector<int> values){
	std::string s;
	for(unsigned int i = 0 ; i < values.size() ; i++){
		s += std::to_string(values.at(i)) += " ";
	}
	return s;
}

bool checkInVector(std::vector<std::string> vector, std::string string){
    for(unsigned int i = 0 ; i < vector.capacity() ; i++){
        if (vector.at(i).find(string) != std::string::npos)
            return true;
    }
    return false;
}

bool myCmp(std::string s1, std::string s2)
{
    if (s1.size() == s2.size()) {return s1 < s2;}
    else {return s1.size() < s2.size();}
}

int main(int argc, char** argv) {
    using namespace indicators;
	std::vector<std::string> folderList;
	for(unsigned int i = 0 ; i < argc ; i++){
		folderList.push_back(argv[i]);
	}
	folderList.erase(folderList.begin());
	int size = folderList.size();
    ProgressBar b1{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(0) + "  : in progress"}};
	ProgressBar b2{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(1) + "  : in progress"}};
	ProgressBar b3{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(2) + "  : in progress"}};
	ProgressBar b4{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(3) + "     : in progress"}};
	ProgressBar b5{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(4) + "     : in progress"}};
	ProgressBar b6{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(5) + "     : in progress"}};
	ProgressBar b7{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(6) + " : in progress"}};
	ProgressBar b8{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(7) + " : in progress"}};
	ProgressBar b9{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(8) + " : in progress"}};
	ProgressBar b10{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(9) + "   : in progress"}};
	ProgressBar b11{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(10) + "   : in progress"}};
	ProgressBar b12{option::BarWidth{50}, option::ForegroundColor{Color::green},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(11) + "   : in progress"}};
	DynamicProgress<ProgressBar> bars(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12);
	bars.set_option(option::HideBarWhenComplete{true});
    #pragma omp parallel for
    for(unsigned int i = 0 ; i < size ; i++){
        std::vector<std::string> folderSplit = split(folderList.at(i), '/');
        std::vector<std::string> fileNames(0);
        std::vector<std::string> bestSolution;
        std::vector<int> fitnessTotal;
        std::vector<int> fitness1;
        std::vector<int> fitness2;
        std::string bestSeed;
        std::string evalType = folderSplit.at(2);
        std::string algorithm = folderSplit.at(3);
        std::string bestGen;
        std::string line;
        int bestFitness = 9999999;
        int bestFitnessTotal = 9999999;
        int bestFitness1 = 9999999;
        int bestFitness2 = 9999999;
        int maxTrials = 0;
        int gen = 0;
        int iEvalType = atoi(evalType.c_str());
        for (const auto & entry : fs::directory_iterator(folderList.at(i))){
            for (const auto & entry : fs::directory_iterator(entry)){
                std::vector<std::string> file = split(entry.path(), '/');
                if((int)std::atoi(file[4].c_str()) > maxTrials)
                    maxTrials = std::atoi(file[4].c_str());
                if (!checkInVector(fileNames, file[5]))
                    fileNames.push_back(file[5]);
            }
        }
        int k  = 0;
        sort(fileNames.begin(), fileNames.end(), myCmp);
        for(unsigned int j = 0 ; j < fileNames.size() ; j++){
            gen++;
            int nbFile = 0;
            int sumFitnessTotal = 0;
            int sumFitness1 = 0;
            int sumFitness2 = 0;
            for (const auto & entry : fs::directory_iterator(folderList.at(i))){
                for (const auto & entry : fs::directory_iterator(entry)){
                    std::vector<std::string> file = split(entry.path(), '/');
                    std::vector<std::string> values;
                    if(fileNames.at(j).find(file[5]) != std::string::npos){
                        std::fstream myFile;
                        myFile.open(file[0] + '/' + file[1] + '/' +  file[2] + "/" + file[3] + "/" + file[4] + "/" + file[5], std::ios::in | std::ios::out);
                        if (myFile.is_open()){
                            nbFile++;
                            getline(myFile, line);
                            boost::split(values, line, boost::is_any_of(" "), boost::token_compress_off);
                            int tmpFitnessTotal = atoi(values.at(0).c_str());
                            int tmpFitness1 = atoi(values.at(1).c_str());
                            int tmpFitness2 = atoi(values.at(2).c_str());
                            int tmpFitness = atoi(values.at(iEvalType).c_str());
                            if(bestFitness > tmpFitness){
                                bestFitness = tmpFitness;
                                bestFitnessTotal = tmpFitnessTotal;
                                bestFitness1 = tmpFitness1;
                                bestFitness2 = tmpFitness2;
                                bestSeed = file[4];
                                bestGen = split(split(file[5], '_')[2], '.')[0];
                                std::vector<std::string>::const_iterator first = values.begin() + 3;
                                std::vector<std::string>::const_iterator last = values.begin() + 28;
                                bestSolution = std::vector<std::string>(first, last);
                                
                            }
                            sumFitnessTotal += tmpFitnessTotal;
                            sumFitness1 += tmpFitness1;
                            sumFitness2 += tmpFitness2;
                            myFile.close();
                        }
                    }
                }
            }
            fitnessTotal.push_back(sumFitnessTotal / nbFile);
            fitness1.push_back(sumFitness1 / nbFile);
            fitness2.push_back(sumFitness2 / nbFile);
            bars[i].set_progress((100 * (j+1))/fileNames.size());
            if (bars[i].is_completed()) {
                bars[i].set_option(option::PrefixText{folderList.at(i) + " : complete"});
                bars[i].mark_as_completed();
            }
        }
        std::ofstream file("../results/data/" + evalType + "_" + algorithm + "_" + ".sav");
        file << concatenateInt(fitnessTotal) << std::endl;
        file << concatenateInt(fitness1) << std::endl;
        file << concatenateInt(fitness2) << std::endl;
        file << "\n" << bestFitnessTotal << " " << bestFitness1 << " " << bestFitness2 << " " << bestSeed << " " << bestGen << std::endl;
        file << concatenate(bestSolution) << std::endl;
        file.close();
    }
    return 0;
}