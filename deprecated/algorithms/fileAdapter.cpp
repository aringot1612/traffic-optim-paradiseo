#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <cstdlib>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <numeric>
#include <eo>
#include "../utils/simulator.h"
#include "omp.h"
#include "../utils/indicators.h"
namespace fs = std::filesystem;

std::vector<std::string> split(std::string text, char delim) {
    std::string line;
    std::vector<std::string> vec;
    std::stringstream ss(text);
    while(std::getline(ss, line, delim)) {
        vec.push_back(line);
    }
    return vec;
}

std::string concatenate(std::vector<std::string> values){
	std::string s;
	for(unsigned int i = 0 ; i < values.size() ; i++){
		s += values.at(i) += " ";
	}
	return s;
}

std::vector<std::vector<std::string>> transformFitness(std::vector<std::vector<std::string>> valuesS, int index){
	std::vector<std::string> valuesString = std::vector<std::string>(valuesS.at(index).end() - 25, valuesS.at(index).end() - 1);
	std::vector<double> values;
	std::vector<double> result;
	for(unsigned int i = 0 ; i < valuesString.size() ; i++){
		values.push_back(atof(valuesString.at(i).c_str()));
	}
	result = Simulator<std::vector<double>>::evaluate(values);
	if(valuesS.at(index).size() == 27){
		valuesS.at(index)[0] = std::to_string((int)round(result[1] + result[2]));
		valuesS.at(index).insert(valuesS.at(index).begin()+1, std::to_string((int)round(result[2])));
		valuesS.at(index).insert(valuesS.at(index).begin()+1, std::to_string((int)round(result[1])));
	}
	else if(valuesS.at(index).size() == 28){
		valuesS.at(index).insert(valuesS.at(index).begin(), std::to_string((int)round((result[1] + result[2]))));
		valuesS.at(index)[1] = std::to_string((int)round(result[1]));
		valuesS.at(index)[2] = std::to_string((int)round(result[2]));
	}
	return valuesS;
}

void transform(const auto & entry, bool multiObj){
	std::string line;
	std::fstream myFile;
	std::string defaultString = "";
	std::vector<std::vector<std::string>> valuesS;
	std::vector<std::vector<std::string>>::iterator it;
	std::vector<std::string> pathT = split(entry.path(), '/');
	int lineNb = 0;
	int index = 0;
	int size = 0;
	myFile.open(entry.path(), std::ios::in | std::ios::out);
	bool needToBeUpdated = true;
	if (myFile.is_open())
	{
		while(getline(myFile, line) && needToBeUpdated)
		{
			if(lineNb == 0){
				if (!line.rfind("\\", 0) == 0)
					needToBeUpdated = false;
			}
			else if(lineNb == 1){
				size = stoi(line);
				valuesS = std::vector<std::vector<std::string>>(size);
			}
			else if(lineNb > 1 && !line.empty()){
				boost::split(valuesS[index], line, boost::is_any_of(" "), boost::token_compress_on);
				valuesS = transformFitness(valuesS, index);
				index++;
			}
			lineNb++;
		}
		myFile.close();
	}
	if(needToBeUpdated){
		int avgTotal = 0;
		int avgF1 = 0;
		int avgF2 = 0;
		int maxTotal = 0;
		int maxF1 = 0;
		int maxF2 = 0;
		int minTotal = 9999999;
		int minF1 = 9999999;
		int minF2 = 9999999;
		int target = 9999999;
		int sumTotal = 0;
		int sumF1 = 0;
		int sumF2 = 0;
		int bestIndex = 0;
		int evalType = atoi(pathT.at(2).c_str());
		std::vector<std::vector<int>> fitness(3, std::vector<int>(size, 0));
		for(unsigned int i = 0 ; i < valuesS.size() ; i++){
			fitness[0][i] = atoi(valuesS[i][0].c_str()); //total
			fitness[1][i] = atoi(valuesS[i][1].c_str()); //time
			fitness[2][i] = atoi(valuesS[i][2].c_str()); //co2
			sumTotal += fitness[0][i];
			sumF1 += fitness[1][i];
			sumF2 += fitness[2][i];
			if(maxTotal < fitness[0][i])
				maxTotal = fitness[0][i];
			if(minTotal > fitness[0][i])
				minTotal = fitness[0][i];
			if(maxF1 < fitness[1][i])
				maxF1 = fitness[1][i];
			if(minF1 > fitness[1][i])
				minF1 = fitness[1][i];
			if(maxF2 < fitness[2][i])
				maxF2 = fitness[2][i];
			if(minF2 > fitness[2][i])
				minF2 = fitness[2][i];

			if(evalType == 0){
				if(target > fitness[0][i]){
					target = fitness[0][i];
					bestIndex = i;
				}
			}
			else if (evalType == 1){
				if(target > fitness[1][i]){
					target = fitness[1][i];
					bestIndex = i;
				}
			}
			else if (evalType == 2){
				if(target > fitness[2][i]){
					target = fitness[2][i];
					bestIndex = i;
				}
			}
		}
		avgTotal = sumTotal / valuesS.size();
		avgF1 = sumF1 / valuesS.size();
		avgF2 = sumF2 / valuesS.size();
		remove(entry.path());
		std::ofstream file (entry.path());
		file << concatenate(valuesS.at(bestIndex)) << "\n" << std::endl;
		file << "fitness avg min max" << std::endl;
		file << "total " << avgTotal << " " << minTotal << " " << maxTotal << std::endl;
		file << "1 " << avgF1 << " " << minF1 << " " << maxF1 << std::endl;
		file << "2 " << avgF2 << " " << minF2 << " " << maxF2 << std::endl;
		file << std::endl;
		for(unsigned int i = 0 ; i < valuesS.size() ; i++){
			file << concatenate(valuesS.at(i)) << std::endl;
		}
		file.close();
	}
}

int main(int argc, char** argv) {
	using namespace indicators;
	std::vector<std::string> folderList;
	for(unsigned int i = 0 ; i < argc ; i++){
		folderList.push_back(argv[i]);
	}
	folderList.erase(folderList.begin());
	int size = folderList.size();
	ProgressBar b1{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(0) + "  : in progress"}};
	ProgressBar b2{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(1) + "  : in progress"}};
	ProgressBar b3{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(2) + "  : in progress"}};
	ProgressBar b4{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(3) + "     : in progress"}};
	ProgressBar b5{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(4) + "     : in progress"}};
	ProgressBar b6{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(5) + "     : in progress"}};
	ProgressBar b7{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(6) + " : in progress"}};
	ProgressBar b8{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(7) + " : in progress"}};
	ProgressBar b9{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(8) + " : in progress"}};
	ProgressBar b10{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(9) + "   : in progress"}};
	ProgressBar b11{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(10) + "   : in progress"}};
	ProgressBar b12{option::BarWidth{50}, option::ForegroundColor{Color::red},option::ShowElapsedTime{true}, option::ShowRemainingTime{true},option::PrefixText{folderList.at(11) + "   : in progress"}};

	DynamicProgress<ProgressBar> bars(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12);
	bars.set_option(option::HideBarWhenComplete{true});
	#pragma omp parallel for
	for(unsigned int i=0; i < size; ++i)
	{
		const std::string folder = folderList[i];
		int count = 0;
		for (const auto & entry : fs::directory_iterator(folderList.at(i))){
			count += std::distance(fs::directory_iterator(entry), fs::directory_iterator());
		}
		int j = 0;
		int multiObj = (folderList.at(i).find("nsgaII") != std::string::npos || folderList.at(i).find("IBEA") != std::string::npos);
		for (const auto & entry : fs::directory_iterator(folderList.at(i))){
			for (const auto & entry : fs::directory_iterator(entry)){
				if(multiObj == 0)
					transform(entry, false);
				else
					transform(entry, true);
				j++;
				bars[i].set_progress((100 * j)/count);
				if (bars[i].is_completed()) {
					bars[i].set_option(option::PrefixText{folderList.at(i) + " : complete"});
					bars[i].mark_as_completed();
				}
			}
		}
	}
	return 0;
}